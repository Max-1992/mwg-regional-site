// function onMouseDown(event) {
// 	console.log(event)
// 	segment = path = null;
// 	var hitResult = project.hitTest(event.point, hitOptions);
// 	if (!hitResult)
// 		return;

// 	if (event.modifiers.shift) {
// 		if (hitResult.type == 'segment') {
// 			hitResult.segment.remove();
// 		};
// 		return;
// 	}

// 	if (hitResult) {
// 		path = hitResult.item;
// 		if (hitResult.type == 'segment') {
// 			segment = hitResult.segment;
// 		} else if (hitResult.type == 'stroke') {
// 			var location = hitResult.location;
// 			segment = path.insert(location.index + 1, event.point);
// 			path.smooth();
// 		}
// 	}
// 	movePath = hitResult.type == 'fill';
// 	if (movePath)
// 		project.activeLayer.addChild(hitResult.item);
// }

// function onMouseMove(event) {

// 	project.activeLayer.selected = false;
// 	if (event.item) {
// 		event.item.selected = true;


// 	}
// }



var lengthArray = [1.120,1.299,1.169,1.069,1,1.100,.9969,1.069,1.269];

//             case 0:
    //                 lengthVariable = 1.120;
    //                 break;
    //             case 1:
    //                 lengthVariable = 1.299;
    //                 break;
    //             case 2:
    //                 lengthVariable = 1.169;
    //                 break;
    //             case 3:
    //                 lengthVariable = 1.069;
    //                 break;
    //             case 4:
    //                 lengthVariable = 1;
    //                 break;
    //             case 5:
    //                 lengthVariable = 1.100;
    //                 break;
    //             case 6:
    //                 lengthVariable = .9969;
    //                 break;
    //             case 7:
    //                 lengthVariable = 1.069;
    //                 break;
    //             case 8:
    //                 lengthVariable = 1.269;
    //                 break;
    //             default:
    //                 lengthVariable = 1;
    //                 break;



// var lengthArray2 = [1.211,1.319,1.219,1.069,1.32,0.99,1.069,1.169,1,1,1.233,1.369,1.369]
// lengthArray2[14] = 1.369;
// lengthArray2[16] = 1.169;
// lengthArray2[21] = 1.369;



var lengthArray3 = [1.09,1.219,1.269,1.069,1.41,1,1,1.069];

lengthArray3[14] = 0.79;
lengthArray3[15] = 0.99;
lengthArray3[16] = 1.35;
lengthArray3[17] = 1.369;
lengthArray3[20] = 1.219;
lengthArray3[21] = 1.219;
lengthArray3[37] = 0.99;
lengthArray3[38] = 0.99;
lengthArray3[39] = 1.169;
lengthArray3[40] = 1.269;
lengthArray3[41] = 1.299;
lengthArray3[42] = 1.369;
lengthArray3[43] = 1.389;
lengthArray3[44] = 1.469;
lengthArray3[45] = 1.469;


lengthArray4 = [1.09,1.219,1.269,1.069,1.21,1,1,1.069,1.30,0.99,1.169,1.38,0.99,1.38,1.169];

lengthArray4[18] = 1.169;
lengthArray4[20] = 1.069;
lengthArray4[21] = 1.079;
lengthArray4[29] = 1.089;
lengthArray4[30] = 1.009;
lengthArray4[34] = 1.069;
lengthArray4[35] = 1.089;
lengthArray4[38] = 1.088;
lengthArray4[39] = 1.233;
lengthArray4[40] = 1.111;
lengthArray4[41] = 1.118;
lengthArray4[44] = 1.218;
lengthArray4[45] = 1.089;



function createBlob4(center, maxRadius, points) {
    var lengthVariable;

    var path = new Path();
    path.closed = true;
    for (var i = 0; i < points; i++) {

        if (false) {
            lengthVariable = 1.38;
        } else {
            switch (i) {
            
       
                case 38:
                    lengthVariable = 1.08;
                    break;
                case 39:
                    lengthVariable = 1.09;
                    break;
                case 40:
                    lengthVariable = 1.11;
                    break;
                case 41:
                    lengthVariable = 1.18;
                    break;
                case 44:
                    lengthVariable = 1.18;
                    break;
                case 45:
                    lengthVariable = 1.08;
                    break;
                default:
                    lengthVariable = 1;
                    break;
            }

        }
        // console.log("index : " + i + " var :" + lengthVariable)
        var delta = new Point({
            length: (maxRadius * 0.7) + (lengthVariable * maxRadius * 0.5),
            angle: (360 / points) * i
        });
        path.add(center + delta);
    }
    path.smooth();
    // 	return path;
    var lightness = 0.00;
    var hue = 0;

    path.strokeWidth = 1;
    path.strokeCap = 'round';
    path.dashArray = [28, 1];
    path.strokeColor = '#48a2df';
    path.opacity = 0.6;
    // console.log(path.segments);
    // console.log("segment 0:")
    // console.log(path.segments[0]._point)


    return path;
}



function getMidPoint(point1, point2) {

    var midX = ((point1.x + point2.x) / 2);
    var midY = ((point1.y + point2.y) / 2);

    return {
        x: midX,
        y: midY
    };
}

