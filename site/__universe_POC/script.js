console.log("load animation")

var canvas = document.getElementById("canvas");

/* Rresize the canvas to occupy the full page, 
   by getting the widow width and height and setting it to canvas*/

// canvas.width = window.innerWidth;
// canvas.height = window.innerHeight;


function OrbitBlob(center, maxRadius, points, lengthArray, lengthVar) {
    this.center = center;
    this.maxRadius = maxRadius;
    this.points = points;
    this.lengthArray = lengthArray;
    this.path = new Path();
    this.path.closed = true;

    for (var i = 0; i < this.points; i++) {
        if (false) {
            lengthVariable = 1;
        } else if (lengthArray[i]) {
            lengthVariable = lengthArray[i];
        } else {
            if (lengthVar) {
                console.log(lengthVar);
                lengthVariable = lengthVar;
            } else {
                lengthVariable = 1;
            }
        }
        console.log(lengthVariable);
        var delta = new Point({
            length: (maxRadius * 0.7) + (lengthVariable * maxRadius * 0.5),
            angle: (360 / points) * i
        });
        this.path.add(center + delta);
    }

    this.path.smooth();

    return this.path
}


var lengthArray1 = [1.220, 1.299, 1.169, 1.069, 1, 1.100, .9969, 1.069, 1.269];

var lengthArray1Alt = [1.380, 0.92, 0.81, 1.169, 0.99, 1.110, 1.269, 1.169, 0.93, 0.93];



var lengthArray2 = [1.211, 1.319, 1.219, 1.069, 1.32, 0.99, 1.069, 1.169, 1, 1, 1.233, 1.369, 1.369]
lengthArray2[14] = 1.369;
lengthArray2[16] = 1.169;
lengthArray2[21] = 1.369;


var lengthArray3 = [1.09, 1.219, 1.269, 1.069, 1.41, 1, 1, 1.069];

lengthArray3[14] = 0.79;
lengthArray3[15] = 0.99;
lengthArray3[16] = 1.35;
lengthArray3[17] = 1.369;
lengthArray3[20] = 1.219;
lengthArray3[21] = 1.219;
lengthArray3[37] = 0.99;
lengthArray3[38] = 0.99;
lengthArray3[39] = 1.169;
lengthArray3[40] = 1.269;
lengthArray3[41] = 1.299;
lengthArray3[42] = 1.369;
lengthArray3[43] = 1.389;
lengthArray3[44] = 1.469;
lengthArray3[45] = 1.469;

var lengthArray4 = [1.09, 1.219, 1.269, 1.069, 1.21, 1, 1, 1.069, 1.20, 0.99, 1.169, 0.91, 0.99, 1.18, 1.169];

lengthArray4[18] = 1.169;
lengthArray4[20] = 1.069;
lengthArray4[21] = 1.079;
lengthArray4[29] = 1.089;
lengthArray4[30] = 1.009;
lengthArray4[34] = 1.069;
lengthArray4[35] = 1.089;
lengthArray4[38] = 1.088;
lengthArray4[39] = 1.233;
lengthArray4[40] = 1.111;
lengthArray4[41] = 1.118;
lengthArray4[44] = 1.218;
lengthArray4[45] = 1.089;



var logoNames = ['weber', 'futurebrand', 'mrm-mccann', 'pmk', 'M', 'mccann-health', 'mccann', 'um', 'craft'];

var rasterArray = [];

var textArray = [];

for (var logoIndex in logoNames) {
    var raster = new Raster(logoNames[logoIndex]);
    raster.opacity = 0.8;
    rasterArray.push(raster);
}

console.log(rasterArray);

var textTitles = ["Media","Entertainment & \nPop Culture",'Corporate \nEntertainment', "Production & \nTranslation", "Healthcare", "Branding & \nDesign", "Public \nRelations", "Experiential & \nShopper", "Digital", "Advertising"];


for (var title in textTitles) {
    textTitles[title] = textTitles[title].toUpperCase();
}


var values = {
    minRadius: 30,
    maxRadius: 90
};

var hitOptions = {
    segments: true,
    stroke: true,
    fill: true,
    tolerance: 5
};

var theBlob = new OrbitBlob(view.size * {
    x: 0.51264,
    y: 0.31111
}, 260, 9, lengthArray1)

theBlob.strokeWidth = 3.0;
theBlob.strokeCap = 'round';
theBlob.dashArray = [0.5, 11];
theBlob.strokeColor = '#fff';

theBlob.rotate(-15);

var theOuterBlob = new OrbitBlob(view.size * {
    x: 0.51264,
    y: 0.31111
}, 560, 9, lengthArray1)

theOuterBlob.rotate(-15);

var theStaticBlob = new OrbitBlob(view.size * {
    x: 0.51264,
    y: 0.31111
}, 260, 9, lengthArray1)

theStaticBlob.rotate(-15);

theOuterBlob.opacity = 0;
theStaticBlob.opacity = 0;

var secondBlob = new OrbitBlob(view.size * {
    x: 0.51264,
    y: 0.31111
}, 260, 22, lengthArray2, 1)

secondBlob.rotate(10);

secondBlob.strokeWidth = 2;
secondBlob.strokeCap = 'round';
secondBlob.dashArray = [1, 14];
secondBlob.strokeColor = '#fff';
secondBlob.opacity = 0.48;

var thirdBlob = new OrbitBlob(view.size * {
    x: 0.50264,
    y: 0.31111
}, 290, 48, lengthArray3, 1.11)

thirdBlob.strokeWidth = 1;
thirdBlob.strokeCap = 'round';
thirdBlob.dashArray = [2, 22];
thirdBlob.strokeColor = '#FFFFAA';
thirdBlob.opacity = 0.5;

var fourthBlob = new OrbitBlob(view.size * {
    x: 0.54264,
    y: 0.31111
}, 270, 48, lengthArray4, 1)

fourthBlob.strokeWidth = 1.2;
fourthBlob.strokeCap = 'round';
fourthBlob.dashArray = [28, 2];
fourthBlob.strokeColor = '#48a2df';
fourthBlob.opacity = 0.4;


innerPointCircles = new Group([]);
outerPointCircles = new Group([]);
outerPointArray = [];

textNodes = new Group([]);
rasterGroup = new Group([]);



project.importSVG('fineCircle.svg', function(item) {
    // item.position = new Point(400, 400);

    for (var i = 0; i < theBlob.segments.length; i++) {

        compoundCircle = item.clone();

        item.visible = false;
        compoundCircle.visible = true;

        console.log(compoundCircle);

        compoundCircle.scale(1.2)

        compoundCircle.position = new Point(theBlob.segments[i].point);
        // console.log(compoundCircle);
        outerPointCircles.addChild(compoundCircle);
        outerPointArray.push(compoundCircle);

        var textLocation;


        var blobCenter = new Point(view.size * {
            x: 0.51264,
            y: 0.31111
        })



        textLocation = blobCenter + theBlob.segments[i].point;

        function addIcon() {
            // console.log(i);
            // console.log(rasterArray);
            // console.log(rasterArray[i]);
            rasterArray[i].position = new Point(theBlob.segments[i].point + (textLocation / 180));
            if (i == 3) {
                rasterArray[i].position += new Point(-50, 15)
            }
            if (i == 6) {
                rasterArray[i].position += new Point(-60, -15)
            }
            if (i == 1 || i == 0) {
                rasterArray[i].position += new Point(60, -5)
            }
            if (i == 8) {
                rasterArray[i].position += new Point(20, -35)
            }
            if (i == 7) {
                rasterArray[i].position += new Point(0, -35)
            }
            if (i == 2) {
                rasterArray[i].position += new Point(50, 15)
            }
            if (i == 4) {
                rasterArray[i].position += new Point(-35, 5)
            }
            if (i == 5) {
                rasterArray[i].position += new Point(-60, -8)
            }
            rasterGroup.addChild(rasterArray[i]);
        }



        function addText() {
            var textpoint = new PointText(theBlob.segments[i].point + (textLocation / 180));
            if (i >= 3 && i <= 5) {
                textpoint.position += new Point(-30, -25)
            }
            if (i == 6) {
                textpoint.position += new Point(-40, -15)
            }
            if (i == 1 || i == 0) {
                textpoint.position += new Point(100, -5)
            }
            if (i == 8) {
                textpoint.position += new Point(40, 0)
            }
            if (i == 7) {
                textpoint.position += new Point(0, -15)
            }
            if (i == 2) {
                textpoint.position += new Point(0, 15)
            }

            textpoint.content = textTitles[i];
            textpoint.fontSize = 12;
            textpoint.fillColor = '#fff';
            textpoint.opacity = 0.6;
            textpoint.justification = 'right';

            textNodes.addChild(textpoint);

            textArray.push(textpoint);

            var text = new PointText(view.size * {
                x: 0.25264,
                y: 0.50354
            });
            text.justification = 'right';
            text.fillColor = '#fff';

            text.fontFamily = 'Helvetica';
            text.fontSize = 48;
        }

        //comment this to remove icons
        addIcon();
        rasterGroup.remove();
        project.activeLayer.addChild(rasterGroup)
            // comment this to remove texts
        addText();
        textNodes.remove();
        // project.activeLayer.addChild(textNodes)
        // textNodes.add();

        var wordsButton = document.getElementById('words');
        var iconsbutton = document.getElementById('icons');

        wordsButton.addEventListener('click', function(event) {
            project.activeLayer.addChild(textNodes);
            rasterGroup.remove();
            theBlob.remove();
            theBlob = new OrbitBlob(view.size * {
                x: 0.51264,
                y: 0.31111
            }, 260, 10, lengthArray1Alt)

            theBlob.strokeWidth = 3.0;
            theBlob.strokeCap = 'round';
            theBlob.dashArray = [0.5, 11];
            theBlob.strokeColor = '#fff';
            theOuterBlob.remove();
            theStaticBlob.remove();
            theOuterBlob = new OrbitBlob(view.size * {
                x: 0.51264,
                y: 0.31111
            }, 560, 10, lengthArray1Alt)
            theStaticBlob = new OrbitBlob(view.size * {
                x: 0.51264,
                y: 0.31111
            }, 260, 10, lengthArray1Alt)
            // theBlob.rotate(-2);
            // theStaticBlob.rotate(-2);
            // theOuterBlob.rotate(-2);
        })

        iconsbutton.addEventListener('click', function(event) {
            project.activeLayer.addChild(rasterGroup);
            textNodes.remove();
            theBlob.remove();
            theBlob = new OrbitBlob(view.size * {
                x: 0.51264,
                y: 0.31111
            }, 260, 9, lengthArray1)

            theBlob.strokeWidth = 3.0;
            theBlob.strokeCap = 'round';
            theBlob.dashArray = [0.5, 11];
            theBlob.strokeColor = '#fff';
            theOuterBlob.remove();
            theStaticBlob.remove();
            theOuterBlob = new OrbitBlob(view.size * {
                x: 0.51264,
                y: 0.31111
            }, 560, 9, lengthArray1)
            theStaticBlob = new OrbitBlob(view.size * {
                x: 0.51264,
                y: 0.31111
            }, 260, 9, lengthArray1)

            theBlob.rotate(-15);
            theOuterBlob.rotate(-15);
            theStaticBlob.rotate(-15);
        })


    }
})

///


var segment, path;
var movePath = false;


var scaleAmount = 1.000092;
var pointSize = {
    x: 0.011264,
    y: 0.011354
};
var flip = 'out';


function moveChance(func) {
    if (Math.random() >= 0.2 || Math.random() <= 0.2) {
        func()
    }
}



/// IDLE ANIMATIONS
function onFrame(event) {
    // theBlob.rotate(0.002)
    // theStaticBlob.rotate(0.002)

    //oldscalevalues 
    // 1.000092
    // 0.99991

    if (event.count > 0 && event.count % 150 == 0) {
        // console.log("DIVISIBLE")
        if (scaleAmount == 1.000092) {
            console.log("in");
            flip = "in";
            scaleAmount = 0.99991;
            pointSize = {
                x: -0.011264,
                y: -0.011354
            };

        } else if (scaleAmount == 0.99991) {
            console.log("out");
            flip = "out";
            scaleAmount = 1.000092;
            pointSize = {
                x: 0.011264,
                y: 0.011354
            };

        }

    }
    moveChance(function() {
        theBlob.scale(scaleAmount, view.size * {
            x: 0.51264,
            y: 0.31111
        })
    })
    moveChance(function() {
        secondBlob.scale(scaleAmount, view.size * {
            x: 0.51264,
            y: 0.31111
        })
    })
    moveChance(function() {
        thirdBlob.scale(scaleAmount, view.size * {
            x: 0.51264,
            y: 0.31111
        })
    })
    moveChance(function() {
        fourthBlob.scale(scaleAmount, view.size * {
            x: 0.51264,
            y: 0.31111
        })
    })


    thirdBlob.rotate(0.06)
    fourthBlob.rotate(-0.02)

    //circle follows the poitn ont he blob, but plus a vector or minus a vector depending on odd or even
    if (event.count > 1) {
        //Points
        for (var i = 0; i < outerPointArray.length; i++) {
            var vector = new Point(view.size * {
                x: 0.51264,
                y: 0.31111
            }) - outerPointArray[i].position;
            if (i % 2 == 0) {
                outerPointArray[i].position = new Point(theBlob.segments[i].point - (vector / 180)) // for (var i in outerPointArray) {
            } else {

                outerPointArray[i].position = new Point(theBlob.segments[i].point + (vector / 180)) // for (var i in outerPointArray) {
            }

        }
        // FOR MAKING WORDS MOVE ONFRAME TOO....
        for (var i = 0; i < rasterArray.length; i++) {
            var vector = new Point(view.size * {
                x: 0.51264,
                y: 0.31111
            }) - rasterArray[i].position;


            rasterArray[i].position = new Point(theBlob.segments[i].point - (vector / 7.8))
            textArray[i].position = new Point(theBlob.segments[i].point - (vector / 7.8))
        }
        ///
    }

}
//


var selectedCircle = {};
var pointOnBlob = {};
var Distance = 0;
var Direction = 'out';
var blobIndex;
var centerLocationXY;
// var centerLocationXY = theOuterBlob.segments[blobIndex].point;
var globalEvent;
var originalLocation;

outerPointCircles.on('mouseenter', function(event) {
    selectedCircle = event.target;
    console.log("selectedCircle");
    console.log(selectedCircle);

    ///THIS IS where the problem is when switching to the svg object. 
    // index always shows up as 0 or 1. why? It needs to be what it was when they were compound paths.
    console.log(theBlob._segments[selectedCircle._index]);
    console.log(selectedCircle._index);
    console.log(selectedCircle._parent._index);

    if (theBlob._segments[selectedCircle._parent._index]._point) {
        pointOnBlob = theBlob._segments[selectedCircle._parent._index]._point;
        blobIndex = theBlob._segments[selectedCircle._parent._index]._index;

        // console.log(blobIndex);
        originalLocation = theStaticBlob._segments[selectedCircle._parent._index]._point;
        centerLocationXY = theOuterBlob.segments[blobIndex].point;

        console.log(centerLocationXY);

    }
    globalEvent = event;
    // console.log(event);
    view.on('frame', touchCircle)
})

outerPointCircles.on('mouseleave', function(event) {

})



var touchCircle = function onFrame(event) {
    return false;

    rasterArray[blobIndex].opacity = 1;

    // Each frame, move the path 1/110th of the difference in position
    // between it and the destination.

    // The vector is the difference between the position of
    // the text item and the destination point:
    // console.log(selectedCircle.position);
    // console.log(centerLocationXY);

    var vector = centerLocationXY - selectedCircle._parent.position;


    // // We add 1/30th of the vector to the position property
    // of the selectedCircle._parent item, to move it in the direction of the
    // centerLocationXY point:
    selectedCircle._parent.position += vector / 80;

    // Set the content of the selectedCircle._parent item to be the length of the vector.
    // I.e. the distance it has to travel still:
    // selectedCircle._parent.content = Math.round(vector.length);
    // console.log(textNodes._children[blobIndex].point)
    if (textNodes._children[blobIndex]) {
        textNodes._children[blobIndex].point += vector / 40;
    }

    if (rasterGroup._children[blobIndex]) {
        rasterGroup._children[blobIndex].position += vector / 40;
    }



    theBlob.segments[blobIndex].point += vector / 60;
    console.log(theBlob.segments)

    if (vector.length < 340) {
        centerLocationXY = originalLocation;


    }
    if (vector.length < 5) {

        centerLocationXY = theOuterBlob.segments[blobIndex].point;

        //This is the line that ends the animation. View turns off the handler here.
        rasterArray[blobIndex].opacity = 0.8;
        view.off('frame', touchCircle)
    }

}

//Maybe make rectangle and have item move around between boundaries of it??


textNodes.on('mousemove', function(event) {
    console.log(event.target.fontSize);
    event.target.opacity = 0.85;
    if (event.target.fontSize < 36) {
        event.target.fontSize += 14;
    }
})

textNodes.on('mouseleave', function(event) {
    event.target.fontSize = 12;
    event.target.opacity = 0.6;
})


rasterGroup.on('mousemove', function(event) {
    event.target.opacity = 1;
})

rasterGroup.on('mouseleave', function(event) {
    event.target.opacity = 0.8;
})



// FOR PLACING THE LOGO
var centerLogo = new Raster('LOGO_MW');

centerLogo.position = new Point(view.size * {
    x: 0.52964,
    y: 0.31111
});
//

// FOR MARKING POINTS IN DEVELOPMENT
function markSegmentPoints(blob) {
    for (var k = 0; k < blob.segments.length; k++) {
        var textpoint = new PointText(blob.segments[k].point);
        textpoint.content = k;
        textpoint.fillColor = "#fadw12";
        textpoint.fontSize = 8;
    }
}
//
// markSegmentPoints(theBlob);
var companiesBox;
var companiesText;
var expertiseBox;

function Boxes() {
    companiesBox = new Path.Rectangle(new Point(view.size * {
        x: 0.79000,
        y: 0.11111
    }), new Size(140, 40))

    companiesBox.fillColor = '#FedF00';
    companiesBox.strokeColor = '#FedF00';

    companiesText = new PointText(companiesBox.center);
    companiesText.content = "content"

    expertiseBox = new Path.Rectangle(new Point(view.size * {
        x: 0.89700,
        y: 0.11111
    }), new Size(140, 40))

    expertiseBox.fillColor = '#7f8c8d';
    expertiseBox.strokeColor = '#7f8c8d';
}

// var tinyCircle = new Path.Circle(new Point(20,20), 3);
// tinyCircle.fillColor = '#f1c40f';
// tinyCircle.opacity = 0.9;


// var biggerCircle = new Path.Circle(new Point(20,20), 8);
// biggerCircle.fillColor = '#ecf0f1';
// biggerCircle.opacity = 0.5;



Boxes()