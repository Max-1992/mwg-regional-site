var coords = [
  new Point(141, 61),
  new Point(278, 42),
  new Point(433, 95),
  new Point(567, 199),
  new Point(523, 444),
  new Point(363, 586),
  new Point(140, 563),
  new Point(64, 420),
  new Point(35, 304),
  new Point(53, 159)
];

var path = new Path({
  strokeColor: 'rgba(255,255,255,1)',
  strokeWidth: 2,
  strokeCap: 'round',
  dashArray: [1, 5],
  closed: true
});
var path2 = new Path({
  strokeColor: 'rgba(255,255,255,0.4)',
  strokeWidth: 1,
  strokeCap: 'round',
  dashArray: [1, 5],
  closed: true
});
var path3 = new Path({
  strokeColor: 'rgba(255,255,255,0.4)',
  strokeWidth: 1,
  strokeCap: 'round',
  dashArray: [1, 8],
  closed: true
});

var heightX = 50;
var heightY = 50;
var handleInCoords = [];
var maxNewX = 0;
var maxNewY = 0;

var CX = 0;
var CY = 0;

for (var i = 0; i < coords.length; i++) {
  path.add(coords[i]);
  path2.add(coords[i]);
  path3.add(coords[i]);
  CX += coords[i].x;
  CY += coords[i].y;
}
CX /= coords.length;
CY /= coords.length;

// var myCircle = new Path.Circle(new Point(CX, CY), 10);
// myCircle.fillColor = 'white';
var xAxis = new Path();
var yAxis = new Path();

xAxis.strokeColor = yAxis.strokeColor = 'red';

xAxis.add(new Point(0,CY), new Point(1000,CY));
yAxis.add(new Point(CX,0), new Point(CX,1000));

path.smooth();
path2.smooth();
path3.smooth();

// path.fullySelected = true;
// path2.fullySelected = true;
// path3.fullySelected = true;

// console.log(path.segments.length);

// var originalCoords = [];
// for (var i = 0; i < path.segments.length; i++) {
//   originalCoords.push({
//     x: path.segments[i].point.x,
//     y: path.segments[i].point.y
//   });
// }

for (var i = 0; i < coords.length; i++) {
  handleInCoords.push({
    x: path.segments[i].handleIn.x,
    y: path.segments[i].handleIn.y
  });
  var el = document.querySelector('.expertise.e' + (i + 1));
  switch (true) {
   case coords[i].x <= CX && coords[i].y <= CY:
    // console.log('q1');
    el.classList.add('q1');
    el.style.left = coords[i].x + 'px';
    el.style.top = coords[i].y + 'px';
    break;
   case coords[i].x > CX && coords[i].y <= CY:
    // console.log('q2');
    el.classList.add('q2');
    el.style.left = coords[i].x + 'px';
    el.style.top = coords[i].y + 'px';
    break;
   case coords[i].x > CX && coords[i].y > CY:
    // console.log('q3');
    el.classList.add('q3');
    el.style.left = coords[i].x + 'px';
    el.style.top = coords[i].y + 'px';
    break;
   case coords[i].x <= CX && coords[i].y > CY:
    // console.log('q4');
    el.classList.add('q4');
    el.style.left = coords[i].x + 'px';
    el.style.top = coords[i].y + 'px';
    break;
  }
  // path2.segments[i].handleOut = new Point(i % 0 ? 50 : -50, i % 0 ? -50 : 50);
}

function onFrame(event) {
  // return
  // console.log('animate');
  // path.add(new Point(100 * Math.random(), 100 * Math.random()));
  // path.rotate(10 * Math.random());
  // return;

  // path.segments[0].handleIn = new Point(10 * event.time, 10 * event.time);
  for (var i = 0; i < path.segments.length; i++) {
    var sinus, sinus2;
    if (i % 2 == 0) {
      sinus = Math.sin(event.count * 0.01 + i);
      sinus2 = Math.cos(event.count * 0.01 + i);
    } else {
      sinus = Math.sin((event.count - 1) * 0.01 + (i - 1));
      sinus2 = Math.cos((event.count - 1) * 0.01 + (i - 1));
    }
      // path.segments[i].handleIn = new Point(sinus * 50, sinus2 * 50 + i*10);
      // path.segments[i].handleOut = new Point(-sinus * 50, -sinus2 * 50 + i*10);
    // console.log(event.count);
    // path.segments[i].handleIn = new Point(sinus, sinus2);
    var offset = 0;
    var m = 2;
    // path.segments[i].handleIn += new Point(m * Math.random() * (event.count % 2 ? -1 : 1) + offset, m * Math.random() * (event.count % 2 ? -1 : 1) + offset);
    // path.segments[i].handleOut += new Point(m * Math.random() * (event.count % 2 ? -1 : 1) + offset, m * Math.random() * (event.count % 2 ? -1 : 1) + offset);

    var newX = sinus * heightX;
    var newY = sinus2 * heightX;
    maxNewX = Math.abs(newX) > maxNewX ? Math.abs(newX) : maxNewX;
    maxNewY = Math.abs(newY) > maxNewY ? Math.abs(newY) : maxNewY;
    // console.log(maxNewX, maxNewY);
    // if (i ==0) console.log(newX, newY);
    // path.segments[i].point.x = newX;
    path.segments[i].handleIn = new Point(handleInCoords[i].x + newX / 4, handleInCoords[i].y + newY / 4);
    // path.rotate(1);
    // path.segments[i].handleOut = new Point(newX / 2, newY / 2);
    // path.segments[i].point.x = Math.abs(coords[i].x + newX / 5);
    // path.segments[i].point.y = Math.abs(coords[i].y + newY / 5);
    // path2.segments[i].point.x = Math.abs(coords[i].x + Math.abs(newX));
    // path2.segments[i].point.y = Math.abs(coords[i].y + Math.abs(newY));
    path3.segments[i].point.x = Math.abs(coords[i].x + newX / 2);
    path3.segments[i].point.y = Math.abs(coords[i].y + newY / 2);
  }
  // if (event.count % 30 === 0){
    // console.log('yo');
    // path.rotate(1);
    path2.rotate(0.02);
    // path3.rotate(1);
    // path3.rotate(1);
  // }
  // console.log(Math.round(event.time * 10));


  // for (var i = 0; i < path.segments.length; i++) {
  //   var segment = path.segments[i];

  //   // A cylic value between -1 and 1
  //   var sinus = Math.sin(event.time * 3 + i);
  //   var sinus2 = Math.sin(event.time * 2 + i);
  //   // debugger;
  //   // Change the y position of the segment point:
  //   segment.point.x = sinus * heightX + 100;
  //   segment.point.y = sinus2 * heightY + 100;
  // }
};
