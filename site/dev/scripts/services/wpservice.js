'use strict';

/**
 * @ngdoc service
 * @name mwgApp.wpservice
 * @description
 * # wpservice
 * Service in the mwgApp.
 */
angular.module('mwgApp')
  .service('wpService', function($rootScope, $http) {
    var wpService = {};

    //Keep a cache of certain feeds so we can preload and don't have to request again
    var newsFeed,
      allNewsFeed,
      featuredNewsFeed,
      featuredWorkFeed,
      workFeed,
      clientFeed,
      peopleFeed,
      expertiseFeed,
      agenciesFeed,
      careersFeed,
      locationsFeed,
      privacy,
      terms,
      truthCentral,
      contactSluts,
      headquarter,
      region,
      address,
      socialMedia,
      regionalMenu,
      aboutContent,
      mcCannDescription,
      hero,
      phone,
      workWithUs,
      allContactModuleInformation,
      allSiteData;

    //Get news (20 per page)
    wpService.getNews = function(numPostsPerPage, pageNum, search) {
        return $http({
          url: $rootScope.baseAPIpath + 'wp-json/wp/v2/news?per_page=' + numPostsPerPage + '&page=' + pageNum + (search ? '&search=' + search : ''),
          method: 'GET',
          dataType: 'json'
        });
    };

    //Get all news
    wpService.getAllNews = function() {
      if (!allNewsFeed) {
        allNewsFeed = $http({
          url: $rootScope.baseAPIpath + 'wp-json/wp/v2/news?per_page=100',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return allNewsFeed;
    };

    //Search for news
    wpService.searchNews = function(numPostsPerPage, pageNum, search) {
      return $http({
        url: $rootScope.baseAPIpath + 'wp-json/wp/v2/news?per_page=' + numPostsPerPage + '&page=' + pageNum + (search ? '&search=' + search : ''),
        method: 'GET',
        dataType: 'json'
      });
    };

    //Get featured news (for the hero carousel)
    wpService.getFeaturedNews = function() {
      if (!featuredNewsFeed) {
        featuredNewsFeed = $http({
          url: $rootScope.baseAPIpath + 'wp-json/wp/v2/news_featured',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return featuredNewsFeed;
    };

     //Get hero
     wpService.getHero = function() {
      if (!hero) {
        hero = $http({
          url: $rootScope.baseAPIpath + 'wp-json/mcannwg/v1/hero',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return hero;
    };

     //Get Truth Central Content
    wpService.getTruthCentralContent = function() {
      if (!truthCentral) {
        truthCentral = $http({
          url: $rootScope.baseAPIpath + 'wp-json/wp/v2/pages?slug=truth-central',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return truthCentral;
    };

    //Get Featured Work Items
    wpService.getFeaturedWorkItems = function() {
      if (!featuredWorkFeed) {
        featuredWorkFeed = $http({
          url: $rootScope.baseAPIpath + 'wp-json/wp/v2/pages?slug=featured-work',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return featuredWorkFeed;
    };

     //Get Privacy Policy
    wpService.getPrivacyPolicy = function() {
      if (!privacy) {
        privacy = $http({
          url: $rootScope.baseAPIpath + 'wp-json/wp/v2/pages?slug=privacy-cookies',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return privacy;
    };

     //Get Terms
    wpService.getTerms = function() {
      if (!terms) {
        terms = $http({
          url: $rootScope.baseAPIpath + 'wp-json/wp/v2/pages?slug=terms-of-use',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return terms;
    };

    //Get Work items
    wpService.getWorkItems = function() {
      if (!workFeed) {
        workFeed = $http({
          url: $rootScope.baseAPIpath + 'wp-json/wp/v2/work?per_page=100',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return workFeed;
    };

    //Get Work detail
    wpService.getWorkDetail = function(slug) {
      return $http({
        url: $rootScope.baseAPIpath + 'wp-json/wp/v2/work?slug=' + slug,
        method: 'GET',
        dataType: 'json',
        error: function(error) {
          alert('erorr', error);
        }
      });
    };

    //Get Client list
    wpService.getClients = function() {
      if (!clientFeed) {
        clientFeed = $http({
          url: $rootScope.baseAPIpath + 'wp-json/wp/v2/clients?filter[orderby]=acf.order&order=desc',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return clientFeed;
    };

    //Get People list
    wpService.getPeople = function() {
      if (!peopleFeed) {
        peopleFeed = $http({
          url: $rootScope.baseAPIpath + 'wp-json/wp/v2/people?filter[orderby]=acf.order&per_page=100',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return peopleFeed;
    };

    //Get Expertise list
    wpService.getExpertise = function() {
      if (!expertiseFeed) {
        expertiseFeed = $http({
          url: $rootScope.baseAPIpath + 'wp-json/wp/v2/expertise',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return expertiseFeed;
    };

   //Get Agencies list
    wpService.getAgencies = function() {
      if (!agenciesFeed) {
        agenciesFeed = $http({
          url: $rootScope.baseAPIpath + 'wp-json/wp/v2/agencies',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return agenciesFeed;
    };

   //Get Careers list
    wpService.getCareers = function() {
      if (!careersFeed) {
        careersFeed = $http({
          url: $rootScope.baseAPIpath + 'wp-json/wp/v2/careers?per_page=100',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return careersFeed;
    };

   //Get Office list
    wpService.getLocations = function() {
      if (!locationsFeed) {
        locationsFeed = $http({
          // url: 'http://dev.mwg.mrmclient.com/offices/api/index.php?table=offices&action=getJson',
          // url: 'https://directoryservice.interpublic.com/mwgoffice/api/?table=offices&action=getJson',
          url: $rootScope.contactAPI,
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return locationsFeed;
    };


    //Get Person detail
    wpService.getPersonDetail = function(id) {
      return $http({
        url: $rootScope.baseAPIpath + 'wp-json/wp/v2/people?slug=' + id,
        method: 'GET',
        dataType: 'json'
      });
    };

    wpService.getContactSluts = function() {
      if (!contactSluts) {
        contactSluts = $http({
          url: $rootScope.baseAPIpath + 'wp-json/mcannwg/v1/contact_slot',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          console.log('getting contact', response )
          return response;
        });
      }
      return contactSluts;
    }

   

    wpService.getHeadquarter = function() {
      if (!headquarter) {
        headquarter = $http({
          url: $rootScope.baseAPIpath + 'wp-json/mcannwg/v1/headquarter',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return headquarter;
    }
    
    wpService.getHeadquarterAddress = function() {
      if (!address) {
        address = $http({
          url: $rootScope.baseAPIpath + 'wp-json/mcannwg/v1/address',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return address;
    }

    wpService.getRegion = function() {
      if (!region) {
        region = $http({
          url: $rootScope.baseAPIpath + 'wp-json/mcannwg/v1/region',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return region;
    }

    wpService.getSocialMediaData = function() {
      if (!socialMedia) {
        socialMedia = $http({
          url: $rootScope.baseAPIpath + 'wp-json/wp/v2/social_media',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          
            return response.data.map( function( socialMedia  ){
              return {
                  name: socialMedia.slug,
                  link: socialMedia.acf.link,
                  label: socialMedia.acf.label
              }
            })
        });
      }
      return socialMedia;
    }

    wpService.getHeadquarterPhone = function() {
      if (!phone) {
        phone = $http({
          url: $rootScope.baseAPIpath + 'wp-json/mcannwg/v1/phone',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return phone;
    }

    /*
    * IMPORTANT. The menu name on wordpress should be named as 'regional'
    * .../wp-backend/wp-admin/nav-menus.php
    */
    wpService.getRegionalMenu = function() {
      if (!regionalMenu) {
        regionalMenu = $http({
          url: $rootScope.baseAPIGlobalPath + 'wp-json/mcannwg/v1/get_menu',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return regionalMenu;
    }

    wpService.getMcCannDescription = function() {
      if (!mcCannDescription) {
        mcCannDescription = $http({
          url: $rootScope.baseAPIGlobalPath + 'wp-json/mcannwg/v1/get_global_data',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return mcCannDescription;
    }

    wpService.getAboutContent = function() {
      if (!aboutContent) {
        aboutContent = $http({
          url: $rootScope.baseAPIpath + 'wp-json/wp/v2/about',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return aboutContent;
    }

    wpService.getWorkWithUsData = function() {
      if (!workWithUs) {
        workWithUs = $http({
          url: $rootScope.baseAPIpath + 'wp-json/mcannwg/v1/workWithUs_data',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return workWithUs;
    }

    wpService.getAllContactModuleInformation = function() {
      if (!allContactModuleInformation) {
        allContactModuleInformation = $http({
          url: $rootScope.baseAPIpath + 'wp-json/mcannwg/v1/getAllContactModuleInformation',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return allContactModuleInformation;
    }

    wpService.getAllSiteData = function() {
      if (!allSiteData) {
        allSiteData = $http({
          url: $rootScope.baseAPIpath + 'wp-json/mcannwg/v1/getAllSiteData',
          method: 'GET',
          dataType: 'json'
        }).then(function(response) {
          return response;
        });
      }
      return allSiteData;
    }

    return wpService;
  });

  