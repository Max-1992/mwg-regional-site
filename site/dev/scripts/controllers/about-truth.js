'use strict';

angular.module('mwgApp')
  .controller('AboutTruthCtrl', function ($scope, wpService) {

    var vm = this;


    wpService.getTruthCentralContent().then(function(response){
      vm.headerText = response.data[0].acf.headers;
      vm.downloads = response.data[0].acf.downloads;
      vm.video_line_1 = response.data[0].acf.video_line_1;
      vm.video_line_2 = response.data[0].acf.video_line_2;
      vm.vimeoUrl = 'https://player.vimeo.com/video/' + response.data[0].acf.vimeo_id;
      vm.watch_video_text = response.data[0].acf.watch_video_text;
    });

  });


