'use strict';

/**
 * @ngdoc function
 * @name mwgApp.controller:ContactCtrl
 * @description
 * # ContactCtrl
 * Controller of the mwgApp
 */
angular.module('mwgApp')
  .controller('ContactCtrl', function ($scope, $rootScope, $route, $window, $sce, wpService, $http, $document, $location) {
  //All variables that are accessible via the HTML view get attached to vm (View Model)
  var vm = this;
  vm.$scope = $scope;
  $scope.$route = $route;


  vm.clickHandler = function(f, r, d, coords) {
    if (vm.infobox) {
      vm.infobox.close();
    }
    if (f.value === 'Global') {
      //vm.map.setCenter(new google.maps.LatLng(38.850033, -40.6500523));
    } else {
      vm.codeAddress(coords, null, null);
    }

    vm.filters[f.name] = f.value;
    vm.resetFilters(r);
    vm.toggleDropdown(d, 'hide');

  };



  vm.codeAddress = function(coords, a, $el) {
    var address = a;
      // map = vm.map,
      // geocoder = g;
    if (address) {
      vm.geocoder.geocode({'address': address}, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
          var ne = results[0].geometry.bounds.getNorthEast();
          var sw = results[0].geometry.bounds.getSouthWest();

          if (MAP_OPTIONS.debug !== false) {
            MAP_OPTIONS.debug(ne, sw);
          }
          map.setCenter(results[0].geometry.location);
          // map.panTo(results[0].geometry.location);
          map.fitBounds(results[0].geometry.bounds);
        } else {
          alert("Geocode was not successful for the following reason: " + status);
        }
      });
    } else if (coords.ne && coords.sw) {
      var ne = new google.maps.LatLng(coords.ne.split(',')[0], coords.ne.split(',')[1]);
      var sw = new google.maps.LatLng(coords.sw.split(',')[0], coords.sw.split(',')[1]);

      if (MAP_OPTIONS.debug !== false) {
        MAP_OPTIONS.debug(ne, sw, vm.map);
      }

      var curr =  {};
      curr.bounds = new google.maps.LatLngBounds(sw, ne);
      curr.center = curr.bounds.getCenter();

      vm.map.setCenter(curr.center);
      // map.panTo(curr.center);
      vm.map.fitBounds(curr.bounds);
    } else if (coords.center) {
      var center = new google.maps.LatLng(coords.center.split(',')[0], coords.center.split(',')[1]);
      // vm.map.setCenter(center);
      vm.map.panTo(center);
      vm.map.setZoom(9);
    } else {
    }
    vm.currentCenter = vm.map.getCenter();
  };

  vm.currentCenter = null;

 
 
  angular.element($window).bind('resize', onResize);
  $scope.$on('$destroy', function() {
    angular.element($window).unbind('resize', onResize);
  });


  vm.filters = {
    regionTop: null,
    region: null,
    country: null,
    city: null
  };

  vm.dropdowns = {
    regionTop: false,
    region: false,
    country: false,
    city: false
  };

  // grab these from global namespace
  // vm.locations = LOCATIONS;
  vm.mapOptions = MAP_OPTIONS;
  // vm.mapOptions.pinImage = $location.$$absUrl + 'images/pin.svg';
  var urlWithoutHash = $location.$$absUrl.replace( "#"+$location.$$hash, "");
  vm.mapOptions.pinImage = {
    url:  urlWithoutHash + 'images/pin.svg',
    scaledSize: new google.maps.Size(50, 50),
  };
  
  vm.mapOptions.dotImage = {
    url: urlWithoutHash + 'images/dot.svg',
    scaledSize: new google.maps.Size(8, 8),
  };

  

  // map options
  // var geocoder,
    // map,
    // mapType = mapType || 'default',
  var boundingBox,
    currentInfoBox = null;

    
  // initialize map
  vm.geocoder = new google.maps.Geocoder();
  var onResize;
  vm.onLoad = function(){
    vm.map = new google.maps.Map(document.getElementById('map_canvas'), vm.mapOptions.mapOptions);

    // Load regional lat & long first
    vm.mapOptions.mapOptions.center = new google.maps.LatLng(25.2048493, 55.2707828);
    vm.map.setOptions({
      styles: vm.mapOptions.mapStylers
    });
    vm.currentCenter = vm.map.getCenter();
    onResize = _.debounce(function() {
      vm.map.panTo(vm.currentCenter);
    }, 100);
    
    $(document).ready( function(){
      google.maps.event.addListener(vm.map, 'tilesloaded', function(evt){
        $(this.getDiv()).find("img").each(function(i, eimg){
          if(!eimg.alt || eimg.alt ===""){
             eimg.alt = "Google Maps Image";
          }
        });
      });
    } )
    
    vm.headquarterAddress = "";
    vm.headquarterPhone = "";
    vm.region = "";
    vm.contactSlot = {};
    wpService.getAllContactModuleInformation().then( function success(response) {
      var coords = {};
      if( response.data ){
          coords.lat =  parseFloat(response.data['headquarter']['coors'].split(',')[0]);
          coords.lng =  parseFloat(response.data['headquarter']['coors'].split(',')[1] );
          vm.map.setCenter( new google.maps.LatLng ( coords.lat + 1, coords.lng));
          vm.headquarterAddress = response.data['headquarter']['address'];
          vm.headquarterPhone   = response.data['headquarter']['phone'];
          vm.region             = response.data['headquarter']['region'];
          vm.contactSlot         = response.data['contact_slot'];
      }
    })

  vm.regionList = [],
  vm.countryList = [],
  vm.cityList = [],
  vm.officeList = [],
  vm.mapMarkers = [];
  vm.infobox = null;


  wpService.getLocations().then(function success(response) {
    vm.locations = response.data;

    var currentCity = '';
    var currentCount = 0;

    _.each(vm.locations, function(item) {
      if(item.headquarters) {
        item.headquarters = "TRUE";
      } else {
        item.headquarters = "";
      }

      if(item.globalHeadquarters) {
        item.globalHeadquarters = "TRUE";
      } else {
        item.globalHeadquarters = "";
      }
    })

    // sort locations on headquarters and city name
    // this is to prevent multiple map markers on the same lat/lng
    vm.locations = _.sortBy(vm.locations, function(item) {
      return [item.city, item.headquarters.trim() ? 'a' : 'b'].join('_');
    });


    _.each(vm.locations, function(item) {

      if (currentCity !== item.city) {
        currentCity = item.city;
        currentCount = 0;
      } else {
        currentCount++;
      }
      var regionBounds = {
        ne: item.r_NELat + ',' + item.r_NELng,
        sw: item.r_SWLat + ',' + item.r_SWLng,
      };
      var countryBounds = {
        ne: item.c_NELat + ',' + item.c_NELng,
        sw: item.c_SWLat + ',' + item.c_SWLng,
      };
      var cityCoords = item.latitude + ',' + item.longitude;
      vm.regionList.push({region: item.region, contactPhone: item.regionContacts, contactEmail: item.contactEmail, bounds: regionBounds || {}, order: item.displayOrder});
      vm.countryList.push({region: item.region/*.replace(/\s+/g, '')*/, country: item.country, bounds: countryBounds || {}});
      vm.cityList.push({country: item.country/*.replace(/\s+/g, '')*/, city: item.city, center: cityCoords || {}});
      vm.officeList.push(item);

      // only add the map marker and events on the first location at lat/lng
      if (currentCount === 0) {
        var marker = new google.maps.Marker({
          map: vm.map,
          position: new google.maps.LatLng(parseFloat(item.latitude) + currentCount / 10, parseFloat(item.longitude) + currentCount / 10, false),
          icon: item.headquarters.trim() ? vm.mapOptions.pinImage : vm.mapOptions.dotImage,
          // title: item.officeName,
          optimized: false,
          city: item.city
        });

        google.maps.event.addListener(marker, 'click', function (marker, content, scope) {

          if (vm.infobox) {
            vm.infobox.close();
          }
          vm.infobox = new InfoBox(vm.mapOptions.infoBoxOptions);

          var content = vm.mapOptions.infoBoxOptions.content
            .join('')
            .replace('{{hq}}', this.headquarters.length ? 'hq' : '')
            .replace('{{city}}', this.city)
            .replace('{{count}}', this.offices.length > 1 ? '(' + this.offices.length + ')' : '');

          vm.infobox.setContent(content);
          vm.infobox.open(vm.map, this);
        });

        google.maps.event.addListener(marker, 'mouseout', function (marker, content, scope) {
          vm.infobox.close();
        });

        // google.maps.event.addListener(marker, 'click', function (marker, content, scope) {
        //   var evtLat = parseFloat(this.position.lat()).toFixed(3),
        //     evtLng = parseFloat(this.position.lng()).toFixed(3);
        //   for (var i = 0; i < vm.officeList.length; i++) {
        //     if (parseFloat(vm.officeList[i].latitude).toFixed(3) == evtLat && parseFloat(vm.officeList[i].longitude).toFixed(3) == evtLng) {
        //       vm.$scope.$apply(function() {
        //         vm.filters.region = vm.officeList[i].region;
        //         vm.filters.country = vm.officeList[i].country;
        //         vm.filters.city = vm.officeList[i].city;
        //         var center = vm.officeList[i].latitude + ', ' + vm.officeList[i].longitude;
        //         vm.codeAddress({center: center}, null, null)
        //       });
        //       break;
        //     }
        //   }
        // });
        vm.mapMarkers.push(marker);
      }


    });

    vm.regionList = _.uniq(vm.regionList, function(item, key, a) {
      return item.region;
    });
    vm.regionList = _.sortBy(vm.regionList, function(item) {
      return item.order;
    });

    vm.countryList = _.uniq(vm.countryList, function(item, key, a) {
      return item.country;
    });
    // _.each(countries, function(item) {
    // });

    vm.cityList = _.uniq(vm.cityList, function(item, key, a) {
      return item.city;
    });
    // _.each(cities, function(item) {
    // });

    for (var i = 0; i < vm.mapMarkers.length; i++) {
      vm.mapMarkers[i].offices = vm.officeList.filter(function(item) {
        return item.city === vm.mapMarkers[i].city;
        // return true;
      });
      vm.mapMarkers[i].headquarters = vm.officeList.filter(function(item) {
        return item.city === vm.mapMarkers[i].city && item.headquarters.trim();
        // return true;
      });
    }

    vm.filters.regionTop = vm.regionList[0].region;
    vm.currentRegion = vm.regionList[0];

   });
  }


  $document.on('click', vm.hideDropdowns);

  $scope.$on('$destroy', function(){
    $document.off('click', vm.hideDropdowns);
  });

  window.vm = vm;
  });
