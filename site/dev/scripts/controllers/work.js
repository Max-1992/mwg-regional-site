'use strict';

/**
 * @ngdoc function
 * @name mwgApp.controller:WorkCtrl
 * @description
 * # WorkCtrl
 * Controller of the mwgApp
 */
angular.module('mwgApp')
  .controller('WorkCtrl', function(wpService, $scope, $window, $rootScope, $location, $timeout) {



    //All variables that are accessible via the HTML view get attached to vm (View Model)
    var vm = this;

    vm.activeIndex = 0;
    vm.activeWorkIndex;
    vm.goPreviousClient = goPreviousClient;
    vm.goNextClient = goNextClient;
    vm.showClient = showClient;
    vm.goToWorkItem = goToWorkItem;

    $rootScope.currentGrouping = false;

    $scope.$emit('work.main', vm);
    vm.locationHistory =$rootScope.history;
    
    vm.featuredWork = "";
    wpService.getFeaturedWorkItems().then(function success(response) {
      // TODO: random only on the first load of the page.
      vm.featuredWork = response.data[0].acf.work_items.slice(0, 6);

      // Random, is the first time loading the page
      if( vm.featuredWork.length > 0 && vm.locationHistory.length <= 1  ){
        vm.featuredWork = _.shuffle(response.data[0].acf.work_items).slice(0, 6);
        localStorage.setItem("works", JSON.stringify( vm.featuredWork) );
      }else{
         // More than 1 history, use the first randomize works
         vm.featuredWork =  JSON.parse( localStorage.getItem("works") );
      }
    });





    // wpService.getWorkItems().then(function success(response) {
    //   vm.work = _.sortBy(response.data, function(o) {
    //     return parseInt(o.acf.order);
    //   });
    // });

    wpService.getClients().then(function success(response) {
      vm.clients = _.sortBy(response.data, function(o) {
        return parseInt(o.acf.order);
      });

      scrollToStoredPosition();
    });

    function goPreviousClient() {
      vm.activeIndex > 0 ? vm.activeIndex -= 1 : vm.activeIndex = 0;

      if (vm.activeClient) {
        vm.activeClient = vm.clients[vm.activeIndex];
      }
    }

    function goNextClient() {
      vm.activeIndex < vm.clients.length - 1 ? vm.activeIndex += 1 : vm.activeIndex = vm.clients.length - 1;

      if (vm.activeClient) {
        vm.activeClient = vm.clients[vm.activeIndex];
      }
    }

    function showClient(client, index) {
      vm.activeClient = client;
      vm.activeIndex = index;

      $('body, html').animate({
        scrollTop: $('.brands_area').offset().top - 150
      }, 'slow');
    }

    function goToWorkItem(slug, activeClient) {
      $rootScope.activeClient = activeClient;
      storeScrollPosition();
      $location.path('/work/' + slug);
    }

    function storeScrollPosition() {
      $rootScope.storedWorkScrollPosition = document.documentElement.scrollTop || document.body.scrollTop;
    }

    function scrollToStoredPosition() {
      if($rootScope.activeClient) {
        vm.activeClient = $rootScope.activeClient;
        vm.activeIndex = _.findIndex(vm.clients, function(client){
          return client.id === vm.activeClient.id;
        });
      }


      if ($rootScope.storedWorkScrollPosition) {
        $timeout(function() {
          document.documentElement.scrollTop = document.body.scrollTop = $rootScope.storedWorkScrollPosition;
          $rootScope.storedWorkScrollPosition = false;
        }, 100);
      }
    }

    //Lazy Load images
    $scope.$on('onRepeatLast', function() {
      initBlazy();
    });

    var bLazy;

    var initBlazy = function() {
      //Lazy Load the Images
      bLazy = new Blazy({
        offset: 100,
        src: 'data-blazy'
      });
    };


    var onResize = _.debounce(function() {
      // bLazy.revalidate();
    }, 250);

    angular.element($window).bind('resize', onResize);

    $scope.$on('$destroy', function() {
      angular.element($window).unbind('resize', onResize);
    });

    vm.scrollUp = function() {
      vm.activeClient = null;
      $('body, html').animate({
        scrollTop: 0
      }, 'slow');

    };

    // wpService.getFeaturedWorkItems().then(function success(response) {
    //   vm.work = response.data[0].acf.work_items;

    //   for (var i = 0; i < vm.work.length; i++) {
    //     if (vm.work[i].id === vm.detail[0].id) {
    //       vm.prevItem = vm.prevItem || vm.work[i - 1];
    //       vm.nextItem = vm.nextItem || vm.work[i + 1];
    //       break;
    //     }
    //   }
    //   console.log('vm.work', vm.work);
    // });


    // console.log('vm.detail', vm.detail);


  });
