'use strict';

/**
 * @ngdoc function
 * @name mwgApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the mwgApp
 */
angular.module('mwgApp')
  .controller('AboutCtrl', function (wpService) {

    //All variables that are accessible via the HTML view get attached to vm (View Model)
    var vm = this;

    //Pre-fetch the leadership people so it loads quick on that page
    wpService.getPeople();
    wpService.getTruthCentralContent();
  });
