'use strict';

/**
 * @ngdoc function
 * @name mwgApp.controller:PrivacyCtrl
 * @description
 * # PrivacyCtrl
 * Controller of the mwgApp
 */
angular.module('mwgApp')
  .controller('PrivacyCtrl', function(wpService, $location, $anchorScroll) {
    //All variables that are accessible via the HTML view get attached to vm (View Model)
    var vm = this;

    vm.scrollTo = function(id) {
      $location.hash(id);
      $anchorScroll();
    }

    wpService.getPrivacyPolicy().then(function(response) {
      console.log('response.data:', response)
      vm.privacy = response.data[0].content.rendered;
    });
  });