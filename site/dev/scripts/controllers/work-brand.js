'use strict';

/**
 * @ngdoc function
 * @name mwgApp.controller:WorkBrandCtrl
 * @description
 * # WorkBrandCtrl
 * Controller of the mwgApp
 */
angular.module('mwgApp')
  .controller('WorkBrandCtrl', function (wpService, $scope, $rootScope, $timeout, $location) {
    //All variables that are accessible via the HTML view get attached to vm (View Model)
    var vm = this;

    vm.slug = 'slug';
    vm.goToWorkItem - goToWorkItem;


    $scope.orderBy = function(item) {
      return parseInt(item.acf.order);
    }

    function goToWorkItem(slug, activeClient) {
      $rootScope.activeClient = activeClient;
      storeScrollPosition();
      $location.path('/work/' + slug);
    }

    function storeScrollPosition() {
      $rootScope.storedWorkScrollPosition = document.documentElement.scrollTop || document.body.scrollTop;
    }

    // wpService.getWorkItems().then(function success(response){
    //   vm.work = response.data;
    //   console.log('vm.work', vm.work);
    // });

    // vm.cilckHandler = function(client) {
      // vm.slug = client;
      // wpService.getClients().then(function success(response){
      //   vm.clients = response.data;
      //   console.log('vm.clients', vm.clients);
      // });
    // };

  });
