'use strict';

/**
 * @ngdoc function
 * @name mwgApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the mwgApp
 */
angular.module('mwgApp')
  .controller('HomeCtrl', function( $location, $scope, $rootScope, wpService, $window, $timeout, anchorSmoothScroll, $anchorScroll) {
    //All variables that are accessible via the HTML view get attached to vm (View Model)
    var vm = this;
    vm.siteData = $rootScope.siteData;

    // Scroll Magic Controllers
    window.controller = null;
    var footerLogoController = null;
    // Scenes
    var sceneFooterLogos,
              sceneIntro,
              sceneWorks,
              sceneExpertise,
              sceneLeadership,
              sceneAbout,
              sceneContact;

            
    $scope.$on('$viewContentLoaded', function(event) {
      
      $timeout(function() {
        $rootScope.scrollOffsetContact = -($(".section_contact h2 span").height() / 2 );
        $rootScope.contactSectionHeight = ( $(".contacts").height()  ) + 100;
        $rootScope.networkHeight    =  $("#network").height() + 100;
        $rootScope.workSectionHeight = ( $(".work_link ").length / 3  ) *  $(".work_link ").height();

        window.controller = new ScrollMagic.Controller();
        footerLogoController = new ScrollMagic.Controller();
        // change behaviour of controller to animate scroll instead of jump
        window.controller.scrollPos(function () {
          if(window.innerWidth >= 720){
            return window.pageYOffset;
          } else {
            return 0;
          }
        });
        
        window.controller.scrollTo(function (newpos) {
          TweenMax.to(window, 0.5, {scrollTo: {y: newpos  }});
        });
      
        // Footer logo scene works on any size.
        sceneFooterLogos = createSceneFooterLogos();
       
        
        if( !toSmall(768) ) {
          initScrollMagic();
        }

        // part of the problem is that window resizes can trigger multiple times as the events fire rapidly
        // this solution prevents the controller from being initialized/destroyed more than once
        


      },1000);
    });

    $(window).resize( function() {
     

      // Force width of intro scene.
      var containerWidth = $('.home_hero ').width();
      $('.home_hero_wrapper').closest('.scrollmagic-pin-spacer').css({ width: containerWidth })
      $('.home_hero_wrapper').css({ width: containerWidth, left: 'auto' })
      
      if( toSmall(768) ) {
      }
    });

    function initScrollMagic() {          
      if( window.sceneIntro === undefined ){
        window.sceneIntro  =  createSceneIntro();
      }else{
        window.sceneIntro[0].destroy(true);
        window.sceneIntro[1].destroy(true);
        window.sceneIntro[2].destroy(true);
        window.sceneIntro  =  createSceneIntro();
      }
      
      if( sceneWorks === undefined ){
        sceneWorks  =  createSceneWorks(window.sceneIntro);
      }else{
        sceneWorks.destroy(true);
        sceneWorks  =  createSceneWorks(window.sceneIntro);
      }

      if( sceneAbout === undefined ){
        sceneAbout  =  createSceneAbout();
      }else{
        sceneAbout.destroy(true);
        sceneAbout  =  createSceneAbout();
      }

      if( sceneExpertise === undefined ){
        sceneExpertise  =  createSceneExpertise();
      }else{
        sceneExpertise.destroy(true);
        sceneExpertise  =  createSceneExpertise();
      }
  }
    

    // Footer logos shimmer
    function createSceneFooterLogos(){
      return new ScrollMagic.Scene({
        triggerElement: '.mccann_logos',
        triggerHook: 'onEnter',
        offset: 20
      }).setClassToggle('.mccann_logos', 'highlight').addTo(footerLogoController)
    }

    // Scene Intro
    function createSceneIntro(){

      var containerWidth = $('.home_hero ').width();
          $('.home_hero_wrapper').css({ width: containerWidth })

      var homeHeroBackground = $(".home_hero_bg_container").height();
      var homeHeroWrapperHeight = ( $(".home_hero_wrapper").height() );
      var heroRegion = ( $(".hero_region").height() / 3 ); 
      
      var scene1 = new ScrollMagic.Scene({  offset: 0,
      triggerHook:0, duration:  140    })
      .setTween( '.home_hero_wrapper', 1, {  y: -140 })
      .addTo(window.controller);
      
      
      var scene2 = new ScrollMagic.Scene({ offset: 0,
        triggerHook:0, duration: homeHeroBackground - 140    })
        .setTween( '.home_hero_content', 1, {  y: '30' })
      .addTo(window.controller);


      var scene3 = new ScrollMagic.Scene({ offset: homeHeroBackground - 140,
        triggerHook:0 })
        .setPin('.home_hero_wrapper', {pushFollowers: false})
       .addTo(window.controller);    

      return [ scene1, scene2, scene3  ];

    }

    // Scene Works
    function createSceneWorks(sceneIntro){
      // Pin works section          
      var workSectionHeight=  ( $(".work_link ").length / 3  ) *  $(".work_link ").height();
      return  new ScrollMagic.Scene({duration: '100%', triggerHook: 0 ,triggerElement: '#workSection',  offset: workSectionHeight })
      .setPin('#workPin', {pushFollowers: false})
      .on('enter', function(){
      })
      .on('leave', function(){
      })
      .addTo(window.controller);
    }

    // Scene about
    function createSceneAbout( sceneWorks ){
      return new ScrollMagic.Scene({
        triggerElement: '#about',
        offset: 0,
        triggerHook:0
      })
      .addTo(window.controller);
    }

    // Section expertise ( amoeba )
    function createSceneExpertise(){
      return new ScrollMagic.Scene({
        triggerElement: '#network',
        offset: $(".expertise-agencies h3").height() ,
        triggerHook:0,
        duration:"100%"
      }).setPin('#expertiseSectionPinWrapper', {pushFollowers: false})
      .addTo(window.controller);
    }

    // Scene contact

    var footerContentStyle = {
      width: '100%',
      zIndex: '2',
      position:'relative',
      paddingTop:'1vw' 
    }

    function createSceneContact(sceneIntro){

      var duration = $rootScope.contactSectionHeight;

      if( toSmall(1200)){
        duration += ( $("#contact > h2").height() );
      }

      return new ScrollMagic.Scene({
        triggerElement: '#contactSection',
        offset: 90,
        triggerHook:0,
        duration: duration
      })
      .setPin('#contactSectionPinWrapper', {pushFollowers: false})
      .setTween( '#contactSectionPinWrapper', 1, {  zIndex: '1' })
      .setTween( '#map_canvas', 1, { height: '0px' })
      .setTween("#footer-content", 1, footerContentStyle )
      .on('enter', function(){
       // sceneIntro[2].removePin();
      })
      .addTo(window.controller);
    }

    function toSmall(max){
      var maxWidth = max; // or whatever your max width is
      if( $(window).width() <= maxWidth ) {
        return true;
      }else{
        return false;
      }
    }


    
    angular.element(document).ready(function () {
      if( $location.hash() === '' ){
        $("html, body").animate({ scrollTop: 0 }, "slow");
      }

      var navBarOffset = 89;
      if( toSmall(768)){
        navBarOffset = 49;
      }

      if( $location.hash() === 'work'){
        $("html, body").animate({ scrollTop: $("#workSection").offset().top - navBarOffset   }, "slow");          
      }
    })

     //Get about content for the carousel
     wpService.getAboutContent().then(function success(response) {
      vm.aboutContent = response.data;
    });

     //Get about content for the carousel
     wpService.getHero().then(function success(response) {
      vm.hero = response.data;
    });

    vm.scrollTo = anchorSmoothScroll.scrollTo;
    wpService.getAgencies().then( function(response){
      vm.agencies = response.data
    });


    wpService.getMcCannDescription().then( function(response){
      vm.mccann_description = response.data
    });
    
  });
