'use strict';

/**
 * @ngdoc function
 * @name mwgApp.controller:CareersCtrl
 * @description
 * # CareersCtrl
 * Controller of the mwgApp
 */
angular.module('mwgApp')
  .controller('CareersCtrl',function($timeout, $sce, wpService, $scope) {
    //All variables that are accessible via the HTML view get attached to vm (View Model)
    var vm = this;
    wpService.getWorkWithUsData().then( function(response ){
      vm.workWithUsData = response.data;
      if( response.data.label === undefined || response.data.label === ""  ){
        vm.workWithUsData.label = "VIEW JOBS";
      }
    })
  });
