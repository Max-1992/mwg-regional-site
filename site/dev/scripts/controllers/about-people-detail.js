'use strict';

/**
 * @ngdoc function
 * @name mwgApp.controller:WorkDetailCtrl
 * @description
 * # WorkDetailCtrl
 * Controller of the mwgApp
 */
angular.module('mwgApp')
  .controller('AboutPeopleDetailCtrl', function($rootScope, $routeParams, wpService, $sce, $scope, $location) {

    // $rootScope.isWork = true;


    //All variables that are accessible via the HTML view get attached to vm (View Model)
    var vm = this;

    vm.prevItem = null;
    vm.nextItem = null;
    vm.closeItem = closeItem;
    vm.currentPerson = null;

    $rootScope.overlayMenu = true;


    // $scope.$on('$locationChangeSuccess', function(event) {
    //   if ($route.current.$$route.controller === 'AboutPeopleDetailCtrl') {
    //     $route.current = lastRoute;
    //     vm.loadPerson();
    //   }
    // });

    var slug = $location.path().split('about/leadership/')[1];

    wpService.getPersonDetail(slug).then(function success(response) {
      vm.person = response.data[0];
      // console.log(vm.person);
    });

    $scope.$on('header.close', function(event, args) {
        closeItem();
    });

    function closeItem() {
        $location.path('/');
        // add scroll to leadership module
    }

    $scope.$emit('about.leadership', vm);

    //If we're coming from  the Work page, skip the DB request
    // if (!$rootScope.currentWorkItem) {
    //   wpService.getWorkDetail($routeParams.slug).then(function success(response) {
    //     processItem(response.data);
    //   });
    // } else {
    //   processItem($rootScope.currentWorkItem);
    // }

    // function processItem(item) {
    //   vm.detail = item;
    //   vm.vimeoUrl = $sce.trustAsResourceUrl(
    //     vm.detail[0].acf.vimeo_id ? 'https://player.vimeo.com/video/' + vm.detail[0].acf.vimeo_id + $rootScope.disableVimeoCookie : null
    //   );

    //   var currentId = vm.detail[0].id,
    //     clientSlug = vm.detail[0].acf.client.slug;

    //   var index;

    //   var title = item[0].acf.client.name + ', ' + item[0].title.rendered + ' | McCann Worldgroup';
    //   var description = item[0].acf.meta_description;
    //   $rootScope.title = title;
    //   $rootScope.description = description;

    //   //Work out the Webring loop

    //   //First check if we're passing in a client (linked from the client section on the page)
    //   if ($rootScope.activeClient) {
    //     index = _.findIndex($rootScope.activeClient.acf.related_work, function(work) {
    //       return work.ID === currentId;
    //     });
    //     prepNextPrev(index, $rootScope.activeClient.acf.related_work);

    //   } else if ($rootScope.currentGrouping) {

    //     index = _.findIndex($rootScope.currentGrouping, function(work) {
    //       return work.id === currentId;
    //     });
    //     prepNextPrev(index, $rootScope.currentGrouping);


    //     //If not, check to see if the item is one of the Featured items
    //   } else if ($rootScope.featuredWork) {
    //     index = _.findIndex($rootScope.featuredWork, function(work) {
    //       return work.ID === currentId;
    //     });

    //     if (index > -1) {
    //       prepNextPrev(index, $rootScope.featuredWork);
    //     } else {

    //       //Finally grab the client list, and figure out which client this item belongs to
    //       wpService.getClients().then(function success(response) {
    //         vm.clients = _.sortBy(response.data, function(o) {
    //           return parseInt(o.acf.order);
    //         });
    //         var clientIndex = _.findIndex(vm.clients, function(client) {
    //           return client.slug === clientSlug;
    //         });

    //         index = _.findIndex(vm.clients[clientIndex].acf.related_work, function(work) {
    //           return work.ID === currentId;
    //         });

    //         prepNextPrev(index, vm.clients[clientIndex].acf.related_work);
    //       });
    //     }
    //   } else {
    //     wpService.getFeaturedWorkItems().then(function success(response) {
    //       vm.featuredWork = _.shuffle(response.data[0].acf.work_items).slice(0,8);
    //       $rootScope.featuredWork = vm.featuredWork;
    //       // vm.featuredWork = _.sortBy(response.data[0].acf.work_items, function(o) {
    //       //   return parseInt(o.acf.order);
    //       // });
    //       index = _.findIndex(vm.featuredWork, function(work) {
    //         return work.ID === currentId;
    //       });

    //       if (index > -1) {
    //         prepNextPrev(index, vm.featuredWork);
    //       } else {

    //         //Finally grab the client list, and figure out which client this item belongs to
    //         wpService.getClients().then(function success(response) {
    //           vm.clients = _.sortBy(response.data, function(o) {
    //             return parseInt(o.acf.order);
    //           });
    //           var clientIndex = _.findIndex(vm.clients, function(client) {
    //             return client.slug === clientSlug;
    //           });

    //           index = _.findIndex(vm.clients[clientIndex].acf.related_work, function(work) {
    //             return work.ID === currentId;
    //           });

    //           prepNextPrev(index, vm.clients[clientIndex].acf.related_work);
    //         });
    //       }
    //     });

    //   }
    // }

    // function prepNextPrev(index, array) {
    //   if (index !== -1) {

    //   }
    //   vm.activeWorkIndex = index;
    //   vm.activeArray = array;
    //   if (index > 0) {
    //     vm.prevItem = vm.activeArray[vm.activeWorkIndex - 1];
    //   } else {
    //     vm.prevItem = false;
    //   }

    //   if (index < vm.activeArray.length - 1) {
    //     vm.nextItem = vm.activeArray[vm.activeWorkIndex + 1];
    //   } else {
    //     vm.nextItem = false;
    //   }

    //   vm.goNext = function() {
    //     if (vm.nextItem) {
    //       if (vm.nextItem.post_name) {
    //         $location.path('/work/' + vm.nextItem.post_name);
    //       } else {
    //         $location.path('/work/' + vm.nextItem.slug);
    //       }
    //     }
    //   }

    //   vm.goPrev = function() {
    //     if (vm.prevItem) {
    //       if (vm.prevItem.post_name) {
    //         $location.path('/work/' + vm.prevItem.post_name);
    //       } else {
    //         $location.path('/work/' + vm.prevItem.slug);
    //       }

    //     }
    //   }

    //   $scope.$on('header.prev', function(event, args) {
    //     vm.goPrev();
    //   });

    //   $scope.$on('header.next', function(event, args) {
    //     vm.goNext();
    //   });

    //   $scope.$on('header.close', function(event, args) {
    //     closeItem();
    //   });


    //   $scope.$emit('work.detail', vm);
    // }

  });
