'use strict';

/**
 * @ngdoc function
 * @name mwgApp.controller:WorkDetailCtrl
 * @description
 * # WorkDetailCtrl
 * Controller of the mwgApp
 */
angular.module('mwgApp')
  .controller('WorkDetailCtrl', ['$rootScope', '$routeParams', 'wpService', '$sce' ,'$scope',  '$location', '$anchorScroll', 'anchorSmoothScroll',  function($rootScope, $routeParams, wpService, $sce, $scope, $location, $anchorScroll, anchorSmoothScroll) {

    $rootScope.isWork = true;
    $("html, body").animate({ scrollTop: 0 }, "slow");

    //All variables that are accessible via the HTML view get attached to vm (View Model)
    var vm = this;

    vm.prevItem = null;
    vm.nextItem = null;
    vm.closeItem = closeItem;
    vm.closeMenu = closeMenu;
    vm.vimeoUrl = null;
    vm.videoActive = false;
    vm.playVideo = playVideo;

    $rootScope.overlayMenu = true;

    //If we're coming from  the Work page, skip the DB request
    if (!$rootScope.currentWorkItem) {
      wpService.getWorkDetail($routeParams.slug).then(function success(response) {
        processItem(response.data);
      });
    } else {
      processItem($rootScope.currentWorkItem);
    }

    function processItem(item) {
      vm.detail = item;
      vm.vimeoUrl = $sce.trustAsResourceUrl(
        vm.detail[0].acf.vimeo_id ? 'https://player.vimeo.com/video/' + vm.detail[0].acf.vimeo_id + $rootScope.disableVimeoCookie : null
      );

      var currentId = vm.detail[0].id,
        clientSlug = vm.detail[0].acf.client.slug;

      var index;

      var title = item[0].acf.client.name + ', ' + item[0].title.rendered + ' | McCann Worldgroup';
      var description = item[0].acf.meta_description;
      $rootScope.title = title;
      $rootScope.description = description;

      //Work out the Webring loop

    
        // I dont know I how much code to do that, But it need to be here.

        wpService.getFeaturedWorkItems().then(function success(response) {

          // The variables should be in plural like this is an array... I dont know who was the developer.
          vm.featuredWork = _.shuffle(response.data[0].acf.work_items).slice(0,8);
          $rootScope.featuredWork = vm.featuredWork;
          // vm.featuredWork = _.sortBy(response.data[0].acf.work_items, function(o) {
          //   return parseInt(o.acf.order);
          // });
          index = _.findIndex(vm.featuredWork, function(work) {
            return work.ID === currentId;
          });

          // If we found works loaded on localstorage, avoid using the latest works found on the previous request.
          if ( index > -1 ) {
            var activeIndex = index;
            var prevNextWorks = [];

            var preLoadedWorks = JSON.parse( localStorage.getItem("works") );
            if( preLoadedWorks ){
              prevNextWorks = preLoadedWorks;

              preLoadedWorks.forEach( function(element, preloadedElementIndex ){
                // I dont know was the developers use a post name, but called it slug on some parts.
                if( element.post_name === $routeParams.slug ){
                  activeIndex = preloadedElementIndex;
                }
              });
            }else{
              prevNextWorks = vm.featuredWork;
            }
            prepNextPrev(activeIndex, prevNextWorks);
          } else {

            //Finally grab the client list, and figure out which client this item belongs to
            wpService.getClients().then(function success(response) {
              vm.clients = _.sortBy(response.data, function(o) {
                return parseInt(o.acf.order);
              });
              var clientIndex = _.findIndex(vm.clients, function(client) {
                return client.slug === clientSlug;
              });

              index = _.findIndex(vm.clients[clientIndex].acf.related_work, function(work) {
                return work.ID === currentId;
              });
              prepNextPrev(index, vm.clients[clientIndex].acf.related_work);
            });
          }
        });
    }

    function prepNextPrev(index, array) {
      if (index !== -1) {
          // What is the pourpouse of that ?! Neither god knows.
      }
      vm.activeWorkIndex = index;
      vm.activeArray = array;
      if (index > 0) {
        vm.prevItem = vm.activeArray[vm.activeWorkIndex - 1];
      } else {
        vm.prevItem = false;
      }

      if (index < vm.activeArray.length - 1) {
        vm.nextItem = vm.activeArray[vm.activeWorkIndex + 1];
      } else {
        vm.nextItem = false;
      }

      vm.goNext = function() {
        if (vm.nextItem) {
          if (vm.nextItem.post_name) {
            $location.path('/work/' + vm.nextItem.post_name);
          } else {
            $location.path('/work/' + vm.nextItem.slug);
          }
        }
      }

      vm.goPrev = function() {
        if (vm.prevItem) {
          if (vm.prevItem.post_name) {
            $location.path('/work/' + vm.prevItem.post_name);
          } else {
            $location.path('/work/' + vm.prevItem.slug);
          }

        }
      }

      $scope.$on('header.prev', function(event, args) {
        vm.goPrev();
      });

      $scope.$on('header.next', function(event, args) {
        vm.goNext();
      });

      $scope.$on('header.close', function(event, args) {
        closeItem();
      });


      $scope.$emit('work.detail', vm);
    }

    function closeItem() {
      if ($rootScope.currentGrouping) {
        $location.path('/expertise/');
      } else {
        $location.path('/');
        $location.hash("work");
      }
    }

    function closeMenu(){
      $scope.mobileMenuOpen = false;
      $scope.regionalMenuOpen = false;
      $scope.mobileAboutMenuOpen = false;
      $location.hash("work");
      $anchorScroll();

    }

    

    function playVideo() {
      vm.videoActive = true;
      var player = new Vimeo.Player($('iframe')[0]);
      player.play();
      // var playBtn =
    }



  }]);
