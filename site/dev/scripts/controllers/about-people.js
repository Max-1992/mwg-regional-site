'use strict';

/**
 * @ngdoc function
 * @name mwgApp.controller:AboutPeopleCtrl
 * @description
 * # AboutPeopleCtrl
 * Controller of the mwgApp
 */
angular.module('mwgApp')
  .controller('AboutPeopleCtrl', function($rootScope, $location, $scope, $route, $timeout, wpService, $window, $document) {

    //All variables that are accessible via the HTML view get attached to vm (View Model)
    var vm = this;


    // prevent the controller from double loading on hash change
    var lastRoute = $route.current;
    $scope.$on('$locationChangeSuccess', function(event) {
      if ($route.current.$$route.controller === 'AboutPeopleCtrl') {
        $route.current = lastRoute;
        vm.loadPerson();
      }
    });

    vm.accordionHidden = false;
    vm.currentPerson = null;
    vm.currentIndex = -1;

    $document.bind('keyup', function handleEscape(event) {
      if (event.which === 27) {
        vm.closeAccordion();
      }
    });



    $(document).ready(function () {
      
    });

    wpService.getPeople().then(function success(response) {
      // vm.people = response.data;
      vm.people = _.sortBy(response.data, function(o) {
        return parseInt(o.acf.order);
      });
      // console.log(vm.people);
    });

    var index;
    var slug;

    vm.loadPerson = function() {
      slug = $location.path().split('about/leadership/')[1];

      if (slug && slug !== '') {

        var filter = vm.people.filter(function(item, i, collection) {
          if (item.slug === slug) {
            index = i;
            return true;
          }
          // return item.slug === slug;
        });

        // console.log('open accordion because hash is defined on load');
        if(vm.people[index].acf.profile_images === '0') {
          return;
        }
        vm.currentPerson = slug;
        vm.accordionHidden = true;
        vm.openAccordion(slug, index);
        // $timeout(function() {
        //   if ($rootScope.isMobile) {
        //     $('body,html').animate({scrollTop: $('#' + slug + '-mobile').position().top - 150} , 'slow');
        //   } else {
        //     // document.getElementById(slug).scrollIntoView();
        //     $('body,html').animate({scrollTop: $('#' + slug).offset().top} , 'slow');
        //   }
        // }, 100);
      } else {
        // console.log('slug is not defined, close accordion');
      }
    };

    vm.delayedSetHeight = function() {
      $timeout(function() {
        if (!$rootScope.isMobile) {
          var insertAfter = parseInt(vm.currentIndex / 3) * 3 + 2;
          var $el = $('.section_about_people .col').eq(insertAfter).length ? $('.section_about_people .col').eq(insertAfter) : $('.section_about_people .col:last');
          vm.$bioElParent.insertAfter($el);
        }
        vm.$bioElParent.height(vm.$bioEl[0].scrollHeight);
      }, 10);
    };

    $scope.$on('header.close', function(event, args) {
      $location.path('/');
    });

    var storedScrollPosition;

    function storeScrollPosition() {
      storedScrollPosition = document.documentElement.scrollTop || document.body.scrollTop;
    }

    function scrollToStoredPosition() {
      $timeout(function(){
        document.documentElement.scrollTop = document.body.scrollTop = storedScrollPosition;
      }, 0);

    }


    //Lazy Load images
    $scope.$on('onRepeatLast', function() {
      initBlazy();

      vm.$bioEl = $('.section_about_bio');
      vm.$bioElContent = $('.section_about_bio > div');
      vm.$bioElParent = $('.section_about_bio').parent();


      // var slug = 'craig-smith';
      // var slug = $location.path().split('/')[1];

      vm.loadPerson();

    });

    var bLazy;

    var initBlazy = function() {
      //Lazy Load the Images
      bLazy = new Blazy({
        offset: 100,
        src: 'data-blazy'
      });
    };

    var windowWidth = $(window).width();

    var onResize = _.debounce(function() {
      if ($(window).width() != windowWidth) {
        //bLazy.revalidate();
        if (slug && $rootScope.isMobile) {
          vm.openAccordion(slug, index);
        }
      }
      if (vm.currentIndex === -1 && !$rootScope.isMobile) {
        vm.currentPerson = null;
        vm.accordionHidden = false;
        $rootScope.$apply();
      } else if (vm.currentPerson && !$rootScope.isMobile) {
        // console.log('reposition element on desktop');
        var insertAfter = parseInt(vm.currentIndex / 3) * 3 + 2;
        var $el = $('.section_about_people .col').eq(insertAfter).length ? $('.section_about_people .col').eq(insertAfter) : $('.section_about_people .col:last');
        vm.$bioElParent.insertAfter($el);
        $rootScope.$emit('about.detail', vm);
      }
      // console.log(vm.currentPerson);
      // console.log(vm.accordionHidden);
      // console.log(vm.currentIndex);
      // console.log('ismobile', $rootScope.isMobile);


    }, 300);

    angular.element($window).bind('resize', onResize);
    $scope.$on('$destroy', function() {
      angular.element($window).unbind('resize', onResize);
      $document.unbind('keyup');
    });


    // wpService.getPeople().then(function success(response){
    //   vm.work = response.data;
    // });
  })
  // .directive('personDetails', function() {
  //   return {
  //     restrict: 'C',
  //     templateUrl: 'views/about-people-detail.html'
  //   };
  // });