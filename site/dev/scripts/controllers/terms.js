'use strict';

/**
 * @ngdoc function
 * @name mwgApp.controller:TermsCtrl
 * @description
 * # TermsCtrl
 * Controller of the mwgApp
 */
angular.module('mwgApp')
  .controller('TermsCtrl', function(wpService) {
    //All variables that are accessible via the HTML view get attached to vm (View Model)
    var vm = this;

    wpService.getTerms().then(function(response) {
      vm.terms = response.data[0].content.rendered;
    });
  });
