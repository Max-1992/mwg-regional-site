'use strict';

/**
 * @ngdoc function
 * @name mwgApp.controller:NewsCtrl
 * @description
 * # NewsCtrl
 * Controller of the mwgApp
 */
angular.module('mwgApp')
  .controller('NewsCtrl', function($routeParams, wpService, $window, $document, $scope) {
    //All variables that are accessible via the HTML view get attached to vm (View Model)
    var vm = this;

    vm.slide = $routeParams.slide;
    vm.searchEnabled = false;
    vm.$input = $('.search_area input');
    vm.$hero = $('.hero');
    vm.loadMore = loadMore;

    var currentPage = 1;

    vm.openSearch = function() {
      vm.searchEnabled = true;
      vm.$input.trigger('focus');
    };

    vm.closeSearch = function() {
      vm.searchEnabled = false;
      vm.query = '';
      if (vm.searchQuery !== '') {
        vm.search(vm.query);
      }
      vm.searchQuery = '';
      currentPage = 1;
      vm.searchMode = false;
      $('.hero').removeClass('search_active');
    };

    vm.search = function(query) {
      wpService.searchNews(20, 1, query).then(function success(response) {
        vm.news = response.data;
        //console.log('vm.news', response);
        vm.queryInput = query;
      });
      vm.$hero.addClass('search_active');
      vm.searchMode = true;
      vm.searchQuery = query;
      currentPage = 1;
    };

    //Call out to the WordPress backend and get a feed of the news
    //wpService.getNews(numberOfPostsPerPage,pageNumber)
    //We can write some logic for lazy loading or pagination using this later
    wpService.getNews(20, 1).then(function success(response) {
      vm.news = response.data;
    });


    //Get the featured news for the carousel
    wpService.getFeaturedNews().then(function success(response) {
      vm.featuredNews = response.data.slice(0,5);
      //console.log('vm.featuredNews', vm.featuredNews);
    });


    function loadMore() {
      if (!vm.searchMode) {
        wpService.getNews(20, currentPage + 1).then(function success(response) {
          vm.news = vm.news.concat(response.data);
          currentPage++;
        });
      } else {
        wpService.searchNews(20, currentPage + 1, vm.searchQuery).then(function success(response) {
          vm.news = vm.news.concat(response.data);
          currentPage++;
        });
      }
    }


    var checkBottom = _.debounce(function() {
      if (angular.element($window).scrollTop() >= angular.element($document).height() - angular.element($window).height() - 400) {
        loadMore();
      }
    }, 250);

    angular.element($window).bind('scroll', checkBottom);

    $scope.$on('$destroy', function() {
      angular.element($window).unbind('scroll');
    });
  });