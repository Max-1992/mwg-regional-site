'use strict';
/**
 * @ngdoc function
 * @name mwgApp.controller:ExpertiseCtrl
 * @description
 * # ExpertiseCtrl
 * Controller of the mwgApp
 */
angular.module('mwgApp')
.controller('ExpertiseCtrl', function($rootScope, $scope, $route, $location, $timeout, wpService) {
  //All variables that are accessible via the HTML view get attached to vm (View Model)
  var vm = this;
  // prevent the controller from double loading on hash change
  var lastRoute = $route.current;
  $scope.$on('$locationChangeSuccess', function(event) {
    if ($route.current.$$route.controller === 'ExpertiseCtrl') {
      $route.current = lastRoute;
    }
  });

  var slug = $location.path().split('expertise/')[1];

  // Setting multi-platform [ competencies ] tab as initial view
  vm.workView = 'multi-platform';
  vm.currentExpertiseIndex = 0;
  vm.currentAgencyIndex = 0;

  vm.$expertise = $('.expertise');
  vm.$agencies = $('.agencies');
  vm.$container = $('.expertise').parent();

  $scope.$on('header.prev', function(event, args) {
    if (vm.workView == 'multi-agency') {
      var i = --vm.currentAgencyIndex;
      vm.currentAgency = vm.agenciesMobile[i].acf.agency_name;
      vm.currentAgencySlug = vm.agenciesMobile[i].slug;
      $rootScope.setPrevNext({cI: vm.currentAgencyIndex, maxI: vm.agenciesMobile.length});
    } else {
      var i = --vm.currentExpertiseIndex;
      vm.currentExpertise = vm.expertiseMobile[i].acf.expertise_name;
      $rootScope.setPrevNext({cI: vm.currentExpertiseIndex, maxI: vm.expertiseMobile.length});
    }
  });

  $scope.$on('header.next', function(event, args) {
    if (vm.workView == 'multi-agency') {
      var i = ++vm.currentAgencyIndex;
      vm.currentAgency = vm.agenciesMobile[i].acf.agency_name;
      vm.currentAgencySlug = vm.agenciesMobile[i].slug;
      $rootScope.setPrevNext({cI: vm.currentAgencyIndex, maxI: vm.agenciesMobile.length});
    } else {
      var i = ++vm.currentExpertiseIndex;
      vm.currentExpertise = vm.expertiseMobile[i].acf.expertise_name;
      $rootScope.setPrevNext({cI: vm.currentExpertiseIndex, maxI: vm.expertiseMobile.length});
    }
  });

  // When click on every point of Competencies tab.
  vm.showExpertise = function(index) {
    $timeout(function() {
      var newHeight = $('.details.ng-enter').height();
      vm.$expertise.height(newHeight);
    }, 100);
    vm.currentExpertiseIndex = index;
    vm.currentExpertise = $rootScope.isMobile ? vm.expertiseMobile[index].acf.expertise_name : vm.expertise[index].acf.expertise_name;
    vm.setWorkView('multi-platform');
    $rootScope.setPrevNext({cI: vm.currentExpertiseIndex, maxI: vm.expertise.length});
    $scope.$emit('expertise.detail', vm);
  };
  // When click on every point of Agencies tab.
  vm.showAgency = function(index) {
    $timeout(function() {
      var newHeight = $('.details.ng-enter').height();
      vm.$agencies.height(newHeight);
    }, 100);
    vm.currentAgencyIndex = index;
    vm.currentAgency = $rootScope.isMobile ? vm.agenciesMobile[index].acf.agency_name : vm.agencies[index].acf.agency_name;
    vm.currentAgencySlug = vm.agencies[index].slug;
    $rootScope.setPrevNext({cI: vm.currentAgencyIndex, maxI: vm.agencies.length});
    $scope.$emit('expertise.detail', vm);
  };

  vm.setWorkView = function(view) {
    if (vm.workView === view) {
      return;
    }
    if( view === 'multi-agency' ){
      $(".expertise-agencies").removeClass( 'multi-platform' ).addClass(view)
    }else{
      $(".expertise-agencies").removeClass( 'multi-agency' ).addClass(view)
    }

    vm.workView = view;
    $timeout(function() {
      $('.featured-work').height($('[class^=multi]:not(.ng-hide)').height());
    }, 200);
    
    paper.view.attachEvent();
  };

  // TODO: It's not used, comment becaouse it will be neccesary
  // $scope.$on('header.close', function(event, args) {
  //   vm.reset();
  // });
  // vm.reset = function() {
  //   vm.currentExpertise = null;
  //   vm.currentAgency = null;
  //   vm.currentAgencySlug = null;
  //   $rootScope.setPrevNext({cI: 0, maxI: 0});
  // };

  // Draw the Amoeba for Competencies and Agencies
  vm.paperJS = function(alt) {
    // wait for contents to load from service and then draw. ( Why dont use a promise instead of timeout ? every time the server response on 500ms. LOL )
    $timeout(function(alt) {
      var canvas = document.getElementById('amoeba');
      canvas.classList.add('visible');
      paper.setup(canvas);
      /**
       * Expertise cooordinates for points
       * return [x, y]
       */
      // Index 0 start on McCann logo and end on MRM// McCann index 7. Index 8 are hidden.
      var _AgencieCoords = [
        [395, 75],		// McCann
        [550, 210],		// FutureBrand
        [590, 393],  	// Weber
        [525, 578],   // UM
        [250, 618], 	// CRAFT
        [52, 475], 		// Momentum
        [55, 265], 		// McCann Health
        [162, 100], 	// MRM-McCann
        [269, 55],		// [hidden]
      ];
      var AgencieCoords = [];
      var i = 0 ;
      _AgencieCoords.forEach( function( coord ){
        // var text = new paper.PointText(new paper.Point(coord[0], coord[1] -15 ));
        // text.justification = 'center';
        // text.fillColor = 'white';
        // text.content = 'Item ' + i;
        AgencieCoords.push( new paper.Point(coord[0], coord[1] -15 ) ) 
        i++
      })

      /**
       * Expertise cooordinates for points
       * return [x, y]
       */
      var _ExpertiseCoords = [
        [160, 78],
        [308, 34],
        [540, 125],
        [587, 280],
        [543, 444],
        [378, 615],
        [187, 542],
        [47, 380],
        [45, 204],
      ];

      var ExpertiseCoords = [];
      _ExpertiseCoords.forEach( function( coord ){
        ExpertiseCoords.push( new paper.Point(coord[0] + 10, coord[1] +10 ) )
      })

      var targetCoords = alt ? AgencieCoords : ExpertiseCoords;
      var moveSteps = 40;
  
      var mainPath = new paper.Path({
        strokeColor: 'rgba(255,255,255,0.8)',
        strokeWidth: 2,
        strokeCap: 'round',
        dashArray: [1, 5],
        closed: true
      });

      var path3 = new paper.Path({
        strokeColor: 'rgba(255,255,255,0.4)',
        strokeWidth: 1.3,
        strokeCap: 'round',
        dashArray: [4, 6],
        closed: true
      });

      var path4 = new paper.Path({
        strokeColor: '#2a464c',
        strokeWidth: 1.5,
        strokeCap: 'round',
        closed: true
      });

      var handleInCoords = [];
      var handleOutCoords = [];
      var maxNewX = 0;
      var maxNewY = 0;
      var CX = 0;
      var CY = 0;
      var points = [];

      // Position of DOM elements represent each point into the amoeba
      var positionElements = function() {
        // determine quadrant for element for styling and position along the path
        // alert('ready')
        for (var i = 0; i < targetCoords.length; i++) {
          var el = document.querySelector('.amoeba-item.e' + (i + 1));
          switch (true) {
            case !el:
              break;
            case targetCoords[i].x <= CX && targetCoords[i].y <= CY:
              // console.log('q1');
              el.classList.add('q1');
              el.classList.add('visible');
              el.style.left = targetCoords[i].x - 3 + 'px';
              el.style.top = targetCoords[i].y - 3 + 'px';
              break;
            case targetCoords[i].x > CX && targetCoords[i].y <= CY:
              // console.log('q2');
              el.classList.add('q2');
              el.classList.add('visible');
              el.style.left = targetCoords[i].x - 5 + 'px';
              el.style.top = targetCoords[i].y - 6 + 'px';
              if( $(el).hasClass('agency-item') ){
                el.style.top = targetCoords[i].y - 2 + 'px';
              }
              if( $(el).is('.expertise-item, .e3') ){
                el.style.top = targetCoords[i].y - 4 + 'px';
              }
              break;
            case targetCoords[i].x > CX && targetCoords[i].y > CY:
              // console.log('q3');
              el.classList.add('q3');
              el.classList.add('visible');
              el.style.left = targetCoords[i].x - 4 + 'px';
              el.style.top = targetCoords[i].y - 6 + 'px';
              if( $(el).is('.expertise-item, .e6') ){
                el.style.top = targetCoords[i].y + 0 + 'px';
              }
              break;
            case targetCoords[i].x <= CX && targetCoords[i].y > CY:
              // console.log('q4');
              el.classList.add('q4');
              el.classList.add('visible');
              el.style.left = targetCoords[i].x - 3 + 'px';
              el.style.top = targetCoords[i].y - 3 + 'px';
              if( $(el).is('.agency-item, .e6') ){
                el.style.top = targetCoords[i].y - 4 + 'px';
              }
              break;
          }
        }
        // reassign target coordinates to the other set
        updateHandleCoords();
        mainPath.smooth();
        targetCoords = targetCoords === AgencieCoords ? ExpertiseCoords : AgencieCoords;
        paper.view.detach('frame', paper.view.move);
      };

      // Determinate the handleIn/Out of segments
      var updateHandleCoords = function() {
        for (var i = 0; i < targetCoords.length; i++) {
          
          handleInCoords.push({
            x: mainPath.segments[i].handleIn.x,
            y: mainPath.segments[i].handleIn.y
          });
          handleOutCoords.push({
            x: mainPath.segments[i].handleOut.x,
            y: mainPath.segments[i].handleOut.y
          });
        }
      };

      for (var i = 0; i < targetCoords.length; i++) {
        mainPath.add(targetCoords[i]);
        path3.add(targetCoords[i]);
        path4.add(targetCoords[i]);
        CX += targetCoords[i].x;
        CY += targetCoords[i].y;
      }

      CX /= targetCoords.length;
      CY /= targetCoords.length;
      
      mainPath.smooth();
      path3.smooth();
      path4.smooth();
      updateHandleCoords();
      positionElements();

      paper.view.attachEvent = function() {
        paper.view.attach('frame', paper.view.move);
      };

      // Move Animation when change between tabs
      paper.view.move = function(event) {
        for (var i = 0; i < mainPath.segments.length; i++) {
          mainPath.segments[i].point.x += (targetCoords[i].x - mainPath.segments[i].point.x) / moveSteps * 2;
          mainPath.segments[i].point.y += (targetCoords[i].y - mainPath.segments[i].point.y) / moveSteps * 2;
          mainPath.smooth();
          path3.segments[i].point.x += (targetCoords[i].x - path3.segments[i].point.x) / moveSteps * 2;
          path3.segments[i].point.y += (targetCoords[i].y - path3.segments[i].point.y) / moveSteps * 2;
          path3.smooth();
          path4.segments[i].point.x += (targetCoords[i].x - path4.segments[i].point.x) / moveSteps * 2;
          path4.segments[i].point.y += (targetCoords[i].y - path4.segments[i].point.y) / moveSteps * 2;
          path4.smooth();
        }

        //check if circle reached its target
        if (Math.round(mainPath.segments[0].point.x) == targetCoords[0].x || Math.round(mainPath.segments[0].point.y) == targetCoords[0].y) {
          positionElements();
        }
      };

      paper.view.onFrame = function(event) {
        for (var i = 0; i < mainPath.segments.length; i++) {
          var sin, cos;
          if (i % 2 == 0) {
            sin = Math.sin(event.count * 0.01 + i);
            cos = Math.cos(event.count * 0.01 + i);
          } else {
            sin = Math.sin((event.count - 1) * 0.01 + (i - 1));
            cos = Math.cos((event.count - 1) * 0.01 + (i - 1));
          }
          var newX = sin / 20;
          var newY = cos / 20;
          maxNewX = Math.abs(newX) > maxNewX ? Math.abs(newX) : maxNewX;
          maxNewY = Math.abs(newY) > maxNewY ? Math.abs(newY) : maxNewY;
          mainPath.segments[i].handleIn = new paper.Point(mainPath.segments[i].handleIn.x + newX, mainPath.segments[i].handleIn.y + newY);
          mainPath.segments[i].handleOut = new paper.Point(mainPath.segments[i].handleOut.x + newX, mainPath.segments[i].handleOut.y + newY);
        }
        var d = Math.round(event.count / 120) % 2 ? 1 : -1;
          path3.rotate(-0.1);
          path4.rotate(0.3);
      };
    }, 500, true, alt);
  };

  // Getting items for the amoeba from WP services.
  angular.element(document).ready( function() {

    wpService.getExpertise().then(function success(response) {
      vm.expertise = response.data.slice();
      vm.expertiseMobile = response.data.sort(function(a, b) {
        return a.acf.order - b.acf.order;
      });
      vm.paperJS(false);
    });
    

    wpService.getAgencies().then(function success(response) {
      vm.agencies = response.data.slice();
      vm.agenciesMobile = response.data.sort(function(a, b) {
        return a.acf.order - b.acf.order;
      });
      
    });
  })
});
