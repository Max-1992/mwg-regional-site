'use strict';
/**
 * @ngdoc overview
 * @name mwgApp
 * @description
 * # mwgApp
 *
 * Main module of the application.
 */
angular
  .module('mwgApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'angular-bind-html-compile',
    'hj.scrollMagic',
    'duScroll'
  ])
  .config(function($routeProvider, $locationProvider, $sceDelegateProvider) {
    $locationProvider.html5Mode(true);

    //Set whitelist for external assets
    $sceDelegateProvider.resourceUrlWhitelist([
      // Allow same origin resource loads.
      'self',
      //Stage backend
      //'http://localhost/mwg-regional-site/wp-backend/**',
			'https://200.110.130.234/mwg-regional-site/wp-backend/**',
      //YouTube
      'http://www.youtube.com/**',
      'https://www.youtube.com/**',
      //Vimeo
      'https://player.vimeo.com/**',
      'http://player.vimeo.com/**'
    ]);

    $routeProvider
      .when('/', {
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl',
        controllerAs: 'vm',
        title: 'McCann Worldgroup | Global Network of Advertising Agencies',
        description: 'McCann Worldgroup is a leading global marketing services company with an integrated network of advertising agencies in over 120 countries'
      })
      .when('/index.html', {
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl',
        controllerAs: 'vm',
        title: 'McCann Worldgroup | Global Network of Advertising Agencies',
        description: 'McCann Worldgroup is a leading global marketing services company with an integrated network of advertising agencies in over 120 countries'
      })
      .when('/home', {
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl',
        controllerAs: 'vm',
        title: 'McCann Worldgroup | Global Network of Advertising Agencies',
        description: 'McCann Worldgroup is a leading global marketing services company with an integrated network of advertising agencies in over 120 countries'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'vm',
        title: 'About | McCann Worldgroup',
        description: 'Meet the McCann Worldgroup leadership team and learn about our unique approach to marketing and how we create cross-discipline, multi-platform, award-winning work for clients'
      })
      .when('/expertise', {
        templateUrl: 'views/expertise.html',
        controller: 'ExpertiseCtrl',
        controllerAs: 'vm',
        title: 'Our Marketing Expertise and Agencies | McCann Worldgroup',
        description: 'McCann Worldgroup is comprised of an integrated network of advertising and specialty marketing agencies, each with a unique expertise'
      })
      .when('/expertise/:slug', {
        templateUrl: 'views/expertise.html',
        controller: 'ExpertiseCtrl',
        controllerAs: 'vm'
      })
      .when('/news', {
        templateUrl: 'views/news.html',
        controller: 'NewsCtrl',
        controllerAs: 'vm',
        title: 'Marketing News and Awards | McCann Worldgroup',
        description: 'The latest in marketing news about the award winning work created by the McCann Worldgroup network of advertising agencies'
      })
      .when('/news/:slide', {
        templateUrl: 'views/news.html',
        controller: 'NewsCtrl',
        controllerAs: 'vm'
      })
      .when('/careers', {
        templateUrl: 'views/careers.html',
        controller: 'CareersCtrl',
        controllerAs: 'vm',
        title: 'Careers and Jobs | McCann Worldgroup',
        description: 'Discover jobs and career opportunities throughout the McCann Worldgroup network of agencies'
      })
      .when('/contact', {
        templateUrl: 'views/contact.html',
        controller: 'ContactCtrl',
        controllerAs: 'vm',
        title: 'Contact | McCann Worldgroup',
        description: 'Get in contact within McCann Worldgroup'
      })
      .when('/privacy', {
        templateUrl: 'views/privacy.html',
        controller: 'PrivacyCtrl',
        controllerAs: 'vm',
        title: 'Privacy Policy | McCann Worldgroup',
      })
      .when('/terms', {
        templateUrl: 'views/terms.html',
        controller: 'TermsCtrl',
        controllerAs: 'vm',
        title: 'Terms of Use | McCann Worldgroup',
      })
      .when('/work', {
        templateUrl: 'views/work.html',
        controller: 'WorkCtrl',
        controllerAs: 'vm'
      })
      .when('/work/:slug', {
        templateUrl: 'views/work-detail.html',
        controller: 'WorkDetailCtrl',
        controllerAs: 'vm'
      })
      .when('/work-brand', {
        templateUrl: 'views/work-brand.html',
        controller: 'WorkBrandCtrl',
        controllerAs: 'vm'
      })
      .when('/about/approach', {
        templateUrl: 'views/about-approach.html',
        controller: 'AboutApproachCtrl',
        controllerAs: 'vm'
      })
      .when('/about/truth', {
        templateUrl: 'views/about-truth.html',
        controller: 'AboutTruthCtrl',
        controllerAs: 'vm',
        title: 'Truth Central | McCann Worldgroup',
        description: 'McCann Worldgroup is dedicated to uncovering the world’s untold truths. These global case studies look at human nature from a fresh perspective'
      })
      .when('/about/leadership/:slug', {
        templateUrl: 'views/about-people-detail.html',
        controller: 'AboutPeopleDetailCtrl',
        controllerAs: 'vm',
        title: 'Leadership | McCann Worldgroup',
        description: 'Meet the McCann Worldgroup leadership team, a collection of marketing experts with cross-discipline fluency'
      })
      .when('/404', {
        templateUrl: 'views/404.html',
        controller: '404Ctrl',
        controllerAs: 'vm',
        title: 'Not Found | McCann Worldgroup'
      })
      .otherwise({
        redirectTo: '/404'
      });
  }).config(function(scrollMagicProvider) {
     //scrollMagicProvider.addIndicators = true;
  })
  .run(function($rootScope, wpService, $window, $location,$http) {

    $http.defaults.headers = {
      'Access-Control-Allow-Origin': '*',
    };
    svg4everybody();

    //Current Year for Footer
    $rootScope.currentYear = new Date().getFullYear();
    $rootScope.gotToHome  = function(){
      $("html, body").animate({ scrollTop: 0 }, "slow");
      $location.path('/').replace();
    }


    // Save the history of navigation
    $rootScope.history = [];

    $rootScope.$on('$routeChangeSuccess', function() {
      $rootScope.history.push($location.$$path);
    });

    $rootScope.back = function () {
        var prevUrl = $rootScope.history.length > 1 ? $rootScope.history.splice(-2)[0] : "/";
        $location.path(prevUrl);
    };

    $rootScope.this_route = function(){
      return $location.path().replace('/', '');
    };

    var protocol = $location.protocol();
    console.log("protocol", protocol)
    // $rootScope.baseAPIpath =  protocol +'://200.110.130.234/mwg-regional-site/wp-backend/';
    // $rootScope.baseAPIGlobalPath = protocol + '://200.110.130.234/mccannworldgroup.com/backend/';
    // $rootScope.contactAPI = 'https://directoryservice.interpublic.com/mwgoffice/api/?table=offices&action=getJson';


    if ($window.location.hostname === 'localhost' || $window.location.href.lastIndexOf('http://stage.mwg.mrmclient.com') !== -1) {
      //$rootScope.baseAPIpath = 'http://localhost/mwg-regional-site/wp-backend/';
      //$rootScope.baseAPIGlobalPath = 'http://localhost/mccannworldgroup.com/backend/';
      
      
      
      //$rootScope.contactAPI = 'https://directoryservicestage.interpublic.com/mwgoffice/api/index.php?table=offices&action=getJson';
      // $rootScope.contactAPI = 'https://170.200.167.43/mwgoffice/api/?table=offices&action=getJson';
    }
    
    // Dont commit       
    $rootScope.baseAPIpath =  'https://200.110.130.234/mwg-regional-site/wp-backend/';
    $rootScope.baseAPIGlobalPath =  'https://200.110.130.234/mccannworldgroup.com/backend/';
    $rootScope.contactAPI = 'https://directoryservice.interpublic.com/mwgoffice/api/?table=offices&action=getJson';
    
    //Prefetch data feeds
    wpService.getExpertise();
    wpService.getAgencies();
    wpService.getRegionalMenu();

    $rootScope.siteData = 
    wpService.getAllSiteData().then( function( response ){
      return response.data;
    })


    // Fetch items for regional top header menu
    wpService.getRegionalMenu().then(function success(response) {
      $rootScope.globalMenuItems = [];
      $rootScope.globalMenuHighlighted = [];
      var host = $location.host();
      var subdomain = "";
      if (host.indexOf('.') >= 0) {
        subdomain = host.split('.')[0];
      }
      // For test. Actually we doesnt have subdomain on stage serve
      subdomain = "uk";
      if( response.data.length > 1 ){
        response.data.forEach( function(element, index){
          var url = element.url;
          element.isCurrentDoamin = false;
          if( url.indexOf(subdomain) > 0 ){
            element.isCurrentDoamin = true;
            $rootScope.globalMenuHighlighted = element;
          }
          $rootScope.globalMenuItems.push(element);          
        });
      }
    });

    // adding shuffle method to Array (mutator)
    Array.prototype.shuffle = function() {
      var i, j, temp = null;
      for (i = this.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[i];
        this[i] = this[j];
        this[j] = temp;
      }
      return this;
    };

    // shuffle elements in a jQuery collection
    $.fn.shuffle = function() {
      var i, j, temp = null;
      for (i = this.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[i];
        this[i] = this[j];
        this[j] = temp;
      }
      return this;
    };


    var checkResize = _.debounce(function() {
      // $rootScope.isMobile = window.getComputedStyle(document.body, ':after').content === '"mobile"';
      if (angular.element($window).width() < 768) {
        $rootScope.isMobile = true;
        $rootScope.isTablet = false;
      } else if (angular.element($window).width() >= 768 && angular.element($window).width() < 1200) {
        $rootScope.isMobile = false;
        $rootScope.isTablet = true;
        $rootScope.scrollOffsetContact = ($(".section_contact h2 span").height() / 2 );
      } else {
        $rootScope.isMobile = false;
        $rootScope.isTablet = false;
      }
      $rootScope.$apply();
    }, 250);


    angular.element($window).bind('resize', checkResize);
    checkResize();

    // Underline header menu item if link area are visible 
    $rootScope.topPath = '';



    $rootScope.mobileSocialFooter = false;
    $rootScope.toggleSocialFooter = function() {
      $rootScope.mobileSocialFooter = !$rootScope.mobileSocialFooter;
    };



    $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
      $rootScope.title = current.$$route.title;
      $rootScope.description = current.$$route.description;


      var dataLayer = window.dataLayer = window.dataLayer || [];

      dataLayer.push({
        'event': 'VirtualPageview',
        'virtualPageURL': window.location.href,
        'virtualPageTitle': $rootScope.title || 'McCann Worldgroup | Global Network of Advertising Agencies'
      });

    });

    $rootScope.scrollOffsetContact = 0;
 
    

    
    //OneTrust Cookie tools
    $rootScope.disableVimeoCookie = '';

    window.OptanonWrapper = function () {
      var cookieValue = decodeURIComponent(readCookie('OptanonConsent')); // Get the users' preferences

      $(document).ready(function(){
        $('#optanon-popup-bottom').children("a").each(function(i, e){
          $(e).attr('aria-label','One Trust Cookie Link')
        })
      })

      

      //Performance Cookies
      if (cookieValue.includes(',2:1,')) { // The '2' is for category 2 (Analytical Cookies), and the '1' means we're looking if it's active.
      } else {
        //Delete existing Google Analytics Cookies
        eraseCookie('_ga');
        eraseCookie('_gat');
        eraseCookie('_gid');
      }


      //Functional Cookies
      if (cookieValue.includes(',3:1,')) { // The '2' is for category 2 (Analytical Cookies), and the '1' means we're looking if it's active.
        //Create a global variable to disable tracking on vimeo players
        $rootScope.disableVimeoCookie = '';
      } else {
        //Create a global variable to disable tracking on vimeo players
        $rootScope.disableVimeoCookie = '?dnt=1';

        setTimeout(function(){
           //Delete existing Vimeo Cookies
            eraseCookie('player');
            eraseCookie('vuid');
          }, 500);
      }

      //Targeting Cookies
      if (cookieValue.includes(',4:1,')) { // The '2' is for category 2 (Analytical Cookies), and the '1' means we're looking if it's active.
      } else {
        //Delete existing Vimeo Cookies
        eraseCookie('IDE');
      }
    };


    function createCookie(name, value, days) {
      if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
      } else var expires = "";
      document.cookie = name + "=" + value + expires + "; path=/";
    }

    function readCookie(name) {
      var nameEQ = name + "=";
      var ca = document.cookie.split(';');
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
      }
      return null;
    }

    function eraseCookie(name) {
      createCookie(name, "", -1);
    }

  })
  .filter('ellipsis', function() {
    return function(value, wordwise, max, tail) {
      if (!value) return '';

      max = parseInt(max, 10);
      if (!max) return value;
      if (value.length <= max) return value;

      value = value.substr(0, max);
      if (wordwise) {
        var lastspace = value.lastIndexOf(' ');
        if (lastspace !== -1) {
          //Also remove . and , so its gives a cleaner result.
          if (value.charAt(lastspace - 1) === '.' || value.charAt(lastspace - 1) === ',') {
            lastspace = lastspace - 1;
          }
          value = value.substr(0, lastspace);
        }
      }

      return value + (tail || '...');
    };
  })
  .directive('agencySvg', function() {
    return function(scope, el, attrs) {
      var url = attrs.agencySvg;
      el.attr('xlink:href', '../images/0_home_logo_' + url + '.svg#' + url);
    }
  }).
  component('socialMedia', {
    templateUrl:'views/social-media.html',
    controller:[ "wpService", "$scope", "$rootScope", function( wpService, $scope, $rootScope) {
      $rootScope.$watch('siteData', function (res) {
        res.then( function(data){
          $scope.instagram = data.footer.instagram;
          $scope.linkedin = data.footer.linkedin;
          $scope.facebook = data.footer.facebook;
        })
      })
    }]
  }).service('anchorSmoothScroll',[ "$window", function($window){


    
    this.scrollTo = function(eID) {
        var isMobile = false;
        if (angular.element($window).width() < 768) {
          isMobile = true;
        }else{
          isMobile = false;
        }

        var startY = currentYPosition();
        var stopY = elmYPosition(eID);
        var distance = stopY > startY ? stopY - startY : startY - stopY;
        if (distance < 100) {
            scrollTo(0, stopY); return;
        }
        var speed = Math.round(distance / 100);
        if (speed >= 20) speed = 30;
        var step = Math.round(distance / 25);
        var leapY = stopY > startY ? startY + step : startY - step;
        var timer = 0;
        if (stopY > startY) {
          for ( var i=startY; i<stopY; i+=step ) {
                if( isMobile ){
                  leapY += 0;
                }else{
                  leapY += -4;
                }
                setTimeout("window.scrollTo(0, "+leapY +")", timer * speed);
                leapY += step; if (leapY > stopY) leapY = stopY; timer++;
            } return;
        }
        for ( var i=startY; i>stopY; i-=step ) {
            if( isMobile ){
              leapY += 0;
            }
            setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
            leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
        }
        
        function currentYPosition() {
            // Firefox, Chrome, Opera, Safari
            if (self.pageYOffset) return self.pageYOffset;
            // Internet Explorer 6 - standards mode
            if (document.documentElement && document.documentElement.scrollTop)
                return document.documentElement.scrollTop;
            // Internet Explorer 6, 7 and 8
            if (document.body.scrollTop) return document.body.scrollTop;
            return 0;
        }
        
        function elmYPosition(eID) {
            var elm = document.getElementById(eID);
            var y = elm.offsetTop;
            var node = elm;
            while (node.offsetParent && node.offsetParent != document.body) {
                node = node.offsetParent;
                y += node.offsetTop;
            } return y;
        }

    };
    
}]).value('duScrollOffset', 90 )
