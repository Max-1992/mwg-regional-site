'use strict';

/**
 * @ngdoc directive
 * @name mwgApp.directive:emitLastRepeat
 * @description
 * # emitLastRepeat
 */
angular.module('mwgApp')
  .directive('emitLastRepeat', function($timeout) {
    return function(scope, element, attrs) {
      if (scope.$last) {
        $timeout(function() {
          scope.$emit('onRepeatLast', element, attrs);
        }, 1);
      }
    };
  });