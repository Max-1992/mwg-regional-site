'use strict';

/**
 * @ngdoc directive
 * @name mwgApp.directive:owlCarousel
 * @description
 * # owlCarousel
 */
angular.module('mwgApp')
.directive("owlCarousel", function() {
  var transient = {};
  function onDragged(event) {
    var owl = event.relatedTarget;
    var draggedCurrent = owl.current();
  
    if (draggedCurrent > this.initialCurrent) {
      owl.current(this.initialCurrent);
      owl.next();
    } else if (draggedCurrent < this.initialCurrent) {
      owl.current(this.initialCurrent);
      owl.prev();
    }
  }

  function onDrag(event) {
    this.initialCurrent = event.relatedTarget.current();
  }


	return {
		restrict: 'E',
		transclude: false,
		link: function (scope) {
			scope.initCarousel = function(element) {
			  // provide any default options you want
				var defaultOptions = {
          items:3,
          slideBy: 'page',
          onDrag: onDrag.bind(transient),
          onDragged: onDragged.bind(transient),
          responsive: {
            1200: {
              items: 4,
              navigation: true,
              nav: true,
              slideBy: 'page'
            }
          }
				};
				var customOptions = scope.$eval($(element).attr('data-options'));
				// combine the two options objects
				for(var key in customOptions) {
					defaultOptions[key] = customOptions[key];
        }
        
				// init carousel
				var curOwl = $(element).data('owlCarousel');
				if(!angular.isDefined(curOwl)) {
  				$(element).owlCarousel(defaultOptions);
        }
        
        $(document).ready(function () {        
            //Go through each carousel on the page
            $('.owl-carousel').each(function() {
              //Find each set of dots in this carousel
              $(this).find('.owl-dot').each(function(index) {
                //Add one to index so it starts from 1
                $(this).attr('aria-label', index + 1);
              });
            });
        
        });
				scope.cnt++;
			};
		}
	};
})
.directive('owlCarouselItem', [function() {
	return {
		restrict: 'A',
		transclude: false,
		link: function(scope, element) {
		  // wait for the last item in the ng-repeat then call init
			if(scope.$last) {
				scope.initCarousel(element.parent());
			}
		}
	};
}]);




/** This is used when there is an ng-repeat so we only initialize the carousel when the data is done populating **/
angular.module('mwgApp').directive('onLastRepeat', function($rootScope) {
  return function(scope, element, attrs) {
    if (scope.$last) setTimeout(function() {
      $rootScope.$broadcast('onLastRepeat', element, attrs);
    }, 10);
  };
});




angular.module('mwgApp')
.directive('owlCarousel1', function ($timeout) {
      return {
        restrict: 'A',
        link: function postLink(scope, element, attrs) {

          scope.initCarousel = function(element) {
            element.owlCarousel({
              items: 1,
              singleItem:true,
              autoplay: false,
              animateOut: 'fadeOut',
              autoplayTimeout: 7000,
              mouseDrag: false,
              touchDrag: false,
              pullDrag: false,
              freeDrag: false,
              dotsContainer: '.carousel-dots',
              loop: true,
              center: true
            })

          }
        }
      }
    }
).directive('owlCarouselItem1', [function() {
	return {
		restrict: 'A',
		transclude: false,
		link: function(scope, element) {
		  // wait for the last item in the ng-repeat then call init
      if(scope.$last) {
				scope.initCarousel(element.parent());
      }
      scope.$on('onRepeatLast', function(scope, element, attrs) {
        scope.initCarousel();
      });
		}
	};
}]);
  


angular.module('mwgApp').directive('onRepeatLast1', function($rootScope) {
  return function(scope, element, attrs) {
    if (scope.$last) setTimeout(function() {
      $rootScope.$broadcast('onRepeatLast', element, attrs);
    }, 10);
  };
});



