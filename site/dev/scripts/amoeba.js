var coords = [
  new Point(141, 61),
  new Point(278, 42),
  new Point(433, 95),
  new Point(567, 199),
  new Point(523, 444),
  new Point(363, 586),
  new Point(140, 563),
  new Point(64, 420),
  new Point(35, 304),
  new Point(53, 159)
];

var path = new Path({
  strokeColor: 'rgba(255,255,255,1)',
  strokeWidth: 2,
  strokeCap: 'round',
  dashArray: [1, 5],
  closed: true
});
var path2 = new Path({
  strokeColor: 'rgba(255,255,255,0.4)',
  strokeWidth: 1,
  strokeCap: 'round',
  dashArray: [1, 5],
  closed: true
});
var path3 = new Path({
  strokeColor: 'rgba(255,255,255,0.4)',
  strokeWidth: 1,
  strokeCap: 'round',
  dashArray: [1, 8],
  closed: true
});

var heightX = 50;
var heightY = 50;
var handleInCoords = [];
var maxNewX = 0;
var maxNewY = 0;
var CX = 0;
var CY = 0;

for (var i = 0; i < coords.length; i++) {
  path.add(coords[i]);
  path2.add(coords[i]);
  path3.add(coords[i]);

  CX += coords[i].x;
  CY += coords[i].y;
}

CX /= coords.length;
CY /= coords.length;

path.smooth();
path2.smooth();
path3.smooth();


for (var i = 0; i < coords.length; i++) {
  handleInCoords.push({
    x: path.segments[i].handleIn.x,
    y: path.segments[i].handleIn.y
  });

  switch (true) {
   case coords[i].x <= CX && coords[i].y <= CY:
    // console.log('q1');
    el.classList.add('q1');
    el.style.left = coords[i].x + 'px';
    el.style.top = coords[i].y + 'px';
    break;
   case coords[i].x > CX && coords[i].y <= CY:
    // console.log('q2');
    el.classList.add('q2');
    el.style.left = coords[i].x + 'px';
    el.style.top = coords[i].y + 'px';
    break;
   case coords[i].x > CX && coords[i].y > CY:
    // console.log('q3');
    el.classList.add('q3');
    el.style.left = coords[i].x + 'px';
    el.style.top = coords[i].y + 'px';
    break;
   case coords[i].x <= CX && coords[i].y > CY:
    // console.log('q4');
    el.classList.add('q4');
    el.style.left = coords[i].x + 'px';
    el.style.top = coords[i].y + 'px';
    break;
  }
}

function onFrame(event) {
  for (var i = 0; i < path.segments.length; i++) {
    var sinus, sinus2;
    if (i % 2 == 0) {
      sinus = Math.sin(event.count * 0.01 + i);
      sinus2 = Math.cos(event.count * 0.01 + i);
    } else {
      sinus = Math.sin((event.count - 1) * 0.01 + (i - 1));
      sinus2 = Math.cos((event.count - 1) * 0.01 + (i - 1));
    }
    var offset = 0;
    var m = 2;
    var newX = sinus * heightX;
    var newY = sinus2 * heightX;
    maxNewX = Math.abs(newX) > maxNewX ? Math.abs(newX) : maxNewX;
    maxNewY = Math.abs(newY) > maxNewY ? Math.abs(newY) : maxNewY;

    path.segments[i].handleIn = new Point(handleInCoords[i].x + newX / 4, handleInCoords[i].y + newY / 4);
    
    path3.segments[i].point.x = Math.abs(coords[i].x + newX / 2);
    path3.segments[i].point.y = Math.abs(coords[i].y + newY / 2);
  }
    path2.rotate(0.02);
};
