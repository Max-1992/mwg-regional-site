function detectIE() {
  var ua = window.navigator.userAgent;

  // Test values; Uncomment to check result

  // IE 10
  // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

  // IE 11
  // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

  // Edge 12 (Spartan)
  // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

  // Edge 13
  // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

  var msie = ua.indexOf('MSIE');
  if (msie > 0) {
    // IE 10 or older => return version number
    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
  }

  var trident = ua.indexOf('Trident');
  if ( trident > -1 ) {
	// IE 11 => return version number
    return true
  }

  var edge = ua.indexOf('Edge/');
  if (edge > 0) {
    // Edge (IE 12+) => return version number
    return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
  }

  // other browser
  return false;
}


var MAP_OPTIONS = {
	'debug': document.location.search.match(/debug/) == null ? false : function debug(ne, sw, m) {
		var boundingBoxPoints = [
			ne, new google.maps.LatLng(ne.lat(), sw.lng()),
			sw, new google.maps.LatLng(sw.lat(), ne.lng()), ne
		],
			map = m;
		window.boundingBox = window.boundingBox || {}
		if (window.boundingBox && window.boundingBox.setMap) {
			window.boundingBox.setMap(null);
		}
		window.boundingBox = new google.maps.Polyline({
			path: boundingBoxPoints,
			strokeColor: '#f00',
			strokeOpacity: 0.5,
			strokeWeight: 2
		});
		window.boundingBox.setMap(map);
	},

	'mapOptions': {
		zoom: 6,
		scrollwheel: false,
		center: new google.maps.LatLng(51.507351, -0.127758),
		mapTypeControlOptions: {
			mapTypeIDs: [google.maps.MapTypeId.HYBRID, google.maps.MapTypeId.TERRAIN],
			style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
		},
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		backgroundColor: '#212121',
		disableDefaultUI:true,
		disableDoubleClickZoom: true
	},
	'mapStylers': [
		{
			featureType: 'all',
			elementType: 'labels',
			stylers: [
				{ visibility: 'off' }
			]
		},
		{
			featureType: 'landscape.natural',
			elementType: 'geometry',
			stylers: [
				{ visibility: 'on' },
				{ color: '#3a3d40' }
			]
		},
		{
			featureType: 'administrative.country',
			elementType: 'geometry.stroke',
			stylers: [
				{ visibility: 'off' }
			]
		},
		{
			featureType: 'poi',
			stylers: [
				{ visibility: 'off' }
			]
		},
		{
			featureType: 'road',
			stylers: [
				{ visibility: 'off' }
			]
		},
		{
		featureType: 'administrative.country',
		elementType: 'labels.text',
		stylers: [
				{
				visibility: 'on'
				},
			]
		},
		{
		featureType: 'administrative.country',
		elementType: 'labels.text.stroke',
		stylers: [
				{
				visibility: 'off'
				},
			]
		},
		{
		featureType: 'administrative.country',
		elementType: 'labels.text.fill',
		stylers: [
				{
					color: '#7d7c7c'
				}
			]
		},
		{
		featureType: 'administrative.locality',
		elementType: 'labels.text',
		stylers: [
				{
				visibility: 'on'
				},
			]
		},
		{
		featureType: 'administrative.locality',
		elementType: 'labels.text.stroke',
		stylers: [
				{
				visibility: 'off'
				},
			]
		},
		{
		featureType: 'administrative.locality',
		elementType: 'labels.text.fill',
		stylers: [
				{
					color: '#7d7c7c'
				}        ]
		},
		{
			featureType: 'administrative',
			elementType: 'geometry',
			stylers: [
				{ visibility: 'off' }
			]
		},
		{
			featureType: 'landscape.man_made',
			stylers: [
				{ visibility: 'off' }
			]
		},
		{
			featureType: 'poi',
			elementType: 'geometry',
			stylers: [
				{ visibility: 'off' }
			]
		},
		{
			featureType: 'water',
			stylers: [
				{ color: '#212121' }
			]
		},
		{
			featureType: 'transit',
			stylers: [
				{ visibility: 'off' }
			]
		}
	],
	'JSONServiceURL': 'office-map.json',
	'JSONServiceNode': 'offices',
	'pinImage': detectIE() ? 'images/pin.png' : 'images/pin.svg',
	'dotImage': detectIE() ? 'images/dot-gray.png' : 'images/dot.svg',
	'infoBoxOptions': {
		content: [
			'<div class="infobox {{hq}}">',
				'<div>Headquarters</div>',
				'<div class="details">{{city}} {{count}}</div>',
			'</div>'
		],
		disableAutoPan: true,
		maxWidth: 0,
		pixelOffset: new google.maps.Size(0, 0),
		zIndex: null,
		closeBoxURL: '',
		infoBoxClearance: new google.maps.Size(1, 1),
		isHidden: false,
		enableEventPropagation: false
	}
};

var LOCATIONS = [
	{
		regionName: 'North America',
		bounds: {
			ne: '71.01695975726373,-57.12890625',
			sw: '15.961329081596647,-165.9375'
		},
		regionContacts: {
			phone: '001 (646) 865-2000',
			email: 'careers@mccann.com'
		},
		countryList: [
			{
				countryName: 'United States',
				bounds: {
					ne: '48.69096039092549,-64.51171875',
					sw: '27.527758206861883,-124.98046875'
				},
				cityList: [
					{
						cityName: 'New York',
						bounds: {
							ne: '40.86212178233553, -73.861083984375',
							sw: '40.69677841595902, -74.04510498046875',
							latitude: 40.7517690,
							longitude: -73.9800218,
						},
						officeList: [
							{
								officeName: 'McCann Group Worldwide Headquarters',
								subtitle: 'office subtitle',
								latitude: 40.7517690,
								longitude: -73.9800218,
								address1: '622 3rd Avenue',
								city: 'New York',
								province: 'NY',
								// country: 'United States',
								zip: '10017',
								phone: '(212) 210-3651',
								email: 'contact@mccann.com',
								headquarters: true,
								globalHeadquarters: true
							}, {
								officeName: 'MRM // McCann New York',
								subtitle: 'office subtitle',
								latitude: 40.7517690,
								longitude: -73.9800218,
								address1: '622 3rd Avenue',
								city: 'New York',
								province: 'NY',
								// country: 'United States',
								zip: '10017',
								phone: '(212) 210-3651',
								email: 'contact@mrm-mccann.com'
							}
						]
					}, {
						cityName: 'Princeton',
						bounds: {
							ne: '40.35230041569844, -74.59373474121094',
							sw: '40.284240077494225, -74.78530883789062',
							latitude: 40.357298,
							longitude: -74.667223,
						},
						officeList: [
							{
								officeName: 'MRM // McCann Princeton',
								subtitle: 'office subtitle',
								latitude: 40.357298,
								longitude: -74.667223,
								address1: '120 Carnegie Drive',
								city: 'Princeton',
								province: 'NJ',
								// country: 'United States',
								zip: '08540',
								phone: '(908) 210-3651'
							}
						]
					}
				]
			}, {
				countryName: 'Canada',
				bounds: {
					ne: '67.80924450600008,-54.66796875',
					sw: '43.45291889355465,-142.03125'
				},
				cityList: [
					{
						cityName: 'Montreal',
						bounds: {
							ne: '45.74836030216746, -73.42987060546875',
							sw: '45.31546044422575, -74.14398193359375',
							latitude: 45.501689,
							longitude: -73.567256,
						},
						officeList: [
							{
								officeName: 'MRM // McCann Montreal',
								city: 'Montreal',
								// country: 'Canada',
								latitude: 45.501689,
								longitude: -73.567256,
								headquarters: true
							}
						]
					}, {
						cityName: 'Quebec',
						bounds: {
							ne: '46.855195, -71.184196',
							sw: '46.795058, -71.269341',
							latitude: 46.813878,
							longitude: -71.207981
						},
						officeList: [
							{
								officeName: 'MRM // McCann Quebec',
								city: 'Quebec',
								// country: 'Canada',
								latitude: 46.813878,
								longitude: -71.207981
							}
						]
					}
				]
			}
		]
	}, {
		regionName: 'Latin America',
		bounds: {
			ne: '27.171582, -46.582031',
			sw: '-20.349777, -110.390625'
		},
		countryList: [
			{
				countryName: 'Mexico',
				bounds: {
					ne: '32.24997445586331,-87.36328125',
					sw: '15.284185114076445,-118.30078125'
				},
				cityList: [
					{
						cityName: 'Cancun',
						bounds: {
							ne: '21.201607, -86.808815',
							sw: '21.125728, -86.902542',
							latitude: 21.161908,
							longitude: -86.851528,
						},
						officeList: [
							{
								officeName: 'MRM // McCann Cancun',
								city: 'Cancun',
								// country: 'Mexico',
								latitude: 21.161908,
								longitude: -86.851528,
								headquarters: true
							}
						]
					}, {
						cityName: 'Mexico City',
						bounds: {
							ne: '19.455819, -99.091530',
							sw: '19.403692, -99.164314',
							latitude: 19.432608,
							longitude: -99.133208
						},
						officeList: [
							{
								officeName: 'MRM // McCann Mexico City',
								city: 'Mexico City',
								// country: 'Mexico',
								latitude: 19.432608,
								longitude: -99.133208
							}
						]
					}
				]
			}
		]
	}, {
		regionName: 'Asia Pacific',
		bounds: {
			ne: '45.301939, 149.238281',
			sw: '-28.502488, 87.539063'
		},
		countryList: [
			{
				countryName: 'Japan',
				bounds: {
					ne: '45.482281, 149.018555',
					sw: '30.248391, 129.902344'
				},
				cityList: [
					{
						cityName: 'Tokyo',
						bounds: {
							ne: '35.793711, 139.879303',
							sw: '35.644864, 139.678802',
							latitude: 35.672778,
							longitude: 139.725305,
						},
						officeList: [
							{
								officeName: 'MRM // McCann Tokyo',
								city: 'Tokyo',
								// country: 'Japan',
								latitude: 35.672778,
								longitude: 139.725305,
								headquarters: true
							}
						]
					}
				]
			}
		]
	}, {
		regionName: 'Europe',
		bounds: {
			ne: '63.222493, 54.228516',
			sw: '37.000359, -8.261719'
		},
		countryList: [
			{
				countryName: 'United Kingdom',
				bounds: {
					ne: '58.470720824119724, 2.9443359375',
					sw: '50.007739014636876, -10.2392578125'
				},
				cityList: [
					{
						cityName: 'London',
						bounds: {
							ne: '51.523210, -0.108833',
							sw: '51.494150, -0.158272',
							latitude: 51.507351,
							longitude: -0.127758,
						},
						officeList: [
							{
								officeName: 'MRM // McCann London',
								city: 'London',
								// country: 'United Kingdom',
								latitude: 51.507351,
								longitude: -0.127758,
								headquarters: true
							}
						]
					}
				]
			}
		]
	}, {
		regionName: 'Middle East',
		bounds: {
			ne: '38.8225909761771,60.8203125',
			sw: '21.69826549685252,24.78515625'
		},
		countryList: [
			{
				countryName: 'Algeria',
				bounds: {
					ne: '33.995750, 7.031250',
					sw: '23.056989, -3.867188'
				},
				cityList: [
					{
						cityName: 'Algers',
						bounds: {
							ne: '36.793813, 3.082695',
							sw: '36.758338, 3.028107',
							latitude: 36.753768,
							longitude: 3.058756
						},
						officeList: [
							{
								officeName: 'MRM // McCann Algers',
								city: 'Algers',
								// country: 'Algeria',
								latitude: 36.753768,
								longitude: 3.058756
							}
						]
					}
				]
			}
		]
	}, {
		regionName: 'Africa',
		bounds: {
			ne: '32.69486597787505,43.59375',
			sw: '-33.13755119234614,-18.28125'
		},
		regionContacts: {
			phone: '123 123 1234',
			email: 'careers@mccann.com'
		},
		countryList: [
			{
				countryName: 'Cameroon',
				bounds: {
					ne: '8.401055, 15.534668',
					sw: '3.309849, 9.667969'
				},
				cityList: [
					{
						cityName: 'Douala',
						bounds: {
							ne: '4.060056, 9.783754',
							sw: '4.038738, 9.748306',
							latitude: 4.051056,
							longitude: 9.767869,
						},
						officeList: [
							{
								officeName: 'MRM // McCann Douala',
								city: 'Douala',
								// country: 'Cameroon',
								latitude: 4.051056,
								longitude: 9.767869,
								headquarters: true
							}
						]
					}
				]
			}
		]
	}
];