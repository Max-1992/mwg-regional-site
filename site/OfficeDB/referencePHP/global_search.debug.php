<?php
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
header ("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header ("Pragma: no-cache"); // HTTP/1.0
header ("Content-Type: text/xml; charset=ISO-8859-1");
error_reporting(E_ALL);

$link="";

global $link;

$server="10.196.176.26";
$username="dirappusr";
$password="g3tDir1nf0";
$database="AD_Datamart";

$auth_user="wsgbldirusr";
$auth_pass="vAGZ#q6M";

print "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n"; 

if (!pam_auth($auth_user, $auth_pass)) {
	print "<error>Invalid authorization</error>\n";
	exit(1);
} 

$link = mssql_connect($server, $username, $password)
or die("Could not connect");

//print "mssql_connect($server, $username, $password);<br>\n";

if ($link) {
	//print "Connected<br>\n";
	mssql_select_db($database);
}  

$valid=false;
$use_GivenName=false;
if ( strpos($_POST["s"]," ") ) {
	list($GivenName, $Surname) = explode(" ", $_POST["s"]);
	$use_GivenName=true;
	if ( ( strlen( $GivenName ) > 0 ) && ( strlen( $Surname ) > 0 ) ) $valid=true;
} else {
	$Surname=$_POST["s"];
	if ( strlen( $Surname ) > 1 )  $valid=true;
}

if (!$valid) {
	print "<warning>Query too short</warning>\n";
	exit(1);
} 
	

if ($valid) {

		if ( $_POST["si"]!="" ) $start=$_POST["si"];
		else $start=1;

		if ( $_POST["ei"]!="" ) $end=$_POST["ei"];
		else $end=20;
		
		$count=($end - $start);
		$total=($start + $end) + 1;

		$Surname=strtolower($Surname);
		$GivenName=strtolower($GivenName);
		
		//print "<warning>start:$start,end:$end,count$count</warning>\n";

		if ( $use_GivenName ) $sql="SELECT * FROM vwADGlobalDirectory WHERE ( LOWER(GivenName)  LIKE LOWER('".$GivenName."%') ) AND ( LOWER(Surname) LIKE LOWER('".$Surname."%') ) ORDER BY Surname ASC";
		else $sql="SELECT * FROM vwADGlobalDirectory WHERE ( LOWER(GivenName) LIKE LOWER('".$Surname."%') ) OR ( LOWER(Surname) LIKE LOWER('".$Surname."%') )  ORDER BY Surname ASC";


		//print "<warning>sql:".$sql."</warning>\n";

		$result=mssql_query( $sql );
		if ( $result ) {
			$current=0;
			print "<records>\n";
			while (($row = mssql_fetch_assoc($result))) {
				++$current;
				if (( $current >= $start ) && ( $current <= $end ) ) {
					//print "<pre>row<br>\n";
					//var_dump( $row );
					//print "</pre><br>\n";
					print "\t<record>\n";
					print "\t<index>".$current."</index>\n"; 					
					print "\t<ObjectGUID>".$row["ObjectGUID"]."</ObjectGUID>\n"; 					
					print "\t<Co>".$row["CountryText"]."</Co>\n"; 					
					print "\t<extensionAttribute8>".$row["CompanyBrand"]."</extensionAttribute8>\n";
					print "\t<BrandName>".$row["s_Brand"]."</BrandName>\n";
					print "\t<GivenName><![CDATA[".$row["Givenname"]."]]></GivenName>\n";
					print "\t<l>".$row["LocalityName"]."</l>\n";
					print "\t<mail>".$row["Mail"]."</mail>\n";
					print "\t<postalCode>".$row["PostalCode"]."</postalCode>\n";
					print "\t<sn><![CDATA[".$row["SurName"]."]]></sn>\n"; 					
					print "\t<state>".$row["State"]."</state>\n"; 					
					print "\t<streetAddress><![CDATA[".$row["StreetAddress"]."]]></streetAddress>\n"; 					
					print "\t<telephoneNumber>".$row["TelephoneNumber"]."</telephoneNumber>\n"; 					
					if ( $_POST["tp"]!="false" ) {
						$img_data="";
						if ( $row["ProfilePicture"]!="" ) $img_data='data:image/png;base64,' . base64_encode($row["ProfilePicture"]);
						print "\t<thumbnailPhoto>".$img_data."</thumbnailPhoto>\n"; 					
					}
					print "\t<title><![CDATA[".$row["Title"]."]]></title>\n"; 					
					print "\t<Manager_GivenName>".$row["Manager_GivenName"]."</Manager_GivenName>\n"; 					
					print "\t<Manager_SurName>".$row["Manager_SurName"]."</Manager_SurName>\n"; 					
					print "\t</record>\n";
				}
			}
			print "</records>\n";
		}
}		
?>
