<?php 

class LinuxAuth { 
    /** @author Marcus Brasizza 
     * 
     * @version 1.0.0 
     */ 
    private $pathShadow = '/etc/shadow'; 
    private $pathPasswd = '/etc/passwd'; 
    private $authConfig = '/etc/sysconfig/authconfig'; 
    private $authConfigTMP = '/tmp/authconfig'; 
    private $content = null; 
    private $useType = null; 
     
     
    /** 
     *  Constructor 
     */ 
    function __construct() { 
        exec('cat  ' . $this->authConfig . ' > '. $this->authConfigTMP );                
        $authCon = file_get_contents($this->authConfigTMP); 
        $strFind = 'USESHADOW'; 
        $tamFind = strlen($strFind); 
        $posShadow = strpos($authCon, $strFind); 
        if ($posShadow !== false) { 
            $useFind = trim(substr($authCon, ($posShadow + $tamFind + 1), 3)); 

            if ($useFind == 'yes') { 
                $this->useType = 'shadow'; 
                exec('cat /etc/shadow > /tmp/shadow');                 
                $file = file('/tmp/shadow'); 
                $this->content = $file; 
                unlink('/tmp/shadow');                    
            } else { 
                $this->useType = 'passwd'; 
                exec('cat /etc/passwd  /tmp/passwd');                 
                $file = file('/tmp/passwd'); 
                $this->content = $file; 
                unlink('/tmp/passwd'); 
            } 
        } 
    } 
     
     
     
    final private function getUserInfo($userInfo) { 
        if (isset($this->content)) { 
            foreach ($this->content as $line) {                
                if (strpos($line, $userInfo) !== false) { 
                    $l = explode(':', $line); 
                    $user = trim($l[0]); 
                    $pass = trim($l[1]);  
                    $info = new stdClass(); 
                    $info->username = $user; 
                    $info->password = $pass; 
                    return $info; 
                }               
                 
            } 
            return false; 
        } 
    } 
     
    /** 
     * Username Access 
     * @param Username $username 
     * @param Password $password 
     * @return boolean true if the user is in the password file system  
     * @access public 
     * @uses $myauth->authUser('myUsername','mypasswordBased64'); 
     */ 
    public function authUser($username,$password){ 
                
        $myInfo = $this->getUserInfo($username); 
        if($myInfo != false){ 
             
            $password = base64_decode($password); 
            
            $passHashed = crypt($password,$myInfo->password); 
            if( trim($passHashed) ===  trim($myInfo->password)){ 
                return true; 
            }else{ 
             return false;   
            } 
        } 
        return false; 
         
    } 

} 
?>  

