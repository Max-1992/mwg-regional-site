if (!String.prototype.trim) {
	String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g, '');};
}

function ajaxTemplate()
{//BEGIN determineSelection
	document.getElementById('main_content').innerHTML = "";
				
	var stringhtml, url;
	url = 'ajax/printCollegeLists.php';
	var xmlHttp = GetXMLHttpObject();
			
	xmlHttp.open("GET", url, false);
	xmlHttp.send(null);
			
	stringhtml = xmlHttp.responseText;

	document.getElementById('main_content').innerHTML = stringhtml;
}//END determineSelection

//GetXMLHttpObject(): Sets up a new xmlhttpobject
function GetXMLHttpObject()
{//BEGIN GetXMLHttpObject
var xmlHttp=null;
try
 {//BEGIN try
	 // Firefox, Opera 8.0+, Safari
	 xmlHttp=new XMLHttpRequest();
 }//END try
catch (e)
 {//BEGIN catch
 //Internet Explorer
 try
  {//BEGIN try
  	xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
  }//END try
 catch (e)
  {//BEGIN catch
	xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
  }//END catch
 }//END catch
return xmlHttp;
}//END GetXMLHttpObject

function getAllFormElements(formName)
{
	if (document.getElementsByTagName) {
		var inputs = document.getElementsByTagName("input");
		
		var selects = document.getElementsByTagName("select");
		
		var textareas = document.getElementsByTagName('textarea'); 

		var getString="";
		
		for (var i=0, end=inputs.length; i<end; i++)
		{			
			if ( inputs[i].type!='checkbox' ) getString = getString + inputs[i].name + "=" + encodeURIComponent(inputs[i].value) + "&"; 
			else if ( inputs[i].checked ) getString = getString + inputs[i].name + "=" + inputs[i].value + "&";
		}
		
		
		for (var x=0, end=selects.length; x<end; x++)
		{			
			getString = getString + selects[x].name + "=" + selects[x].value + "&"; 
		}
		
		for (var i=0, end=textareas.length; i<end; i++)
		{			
			getString = getString + textareas[i].name + "=" + textareas[i].value + "&"; 
		}

		return getString;
		
	}
}

var busy=false;
last_query="";

function searchNames(){
	var formName="test-form";
	//url = 'http://170.200.167.207/test/global_search.php';
	url = 'https://directoryservice.interpublic.com/test/global_search.php';

       
	search_name = document.getElementById('s').value;
	search_name = search_name.trim();
	if ((search_name.length < 2 ) || ( busy ) || ( search_name==last_query ) ) return;
	
	//alert( 'search_name:' + search_name );

	last_query=search_name;
	
	busy=true;
	url = url + "?" + getAllFormElements(formName);
       //alert( 'url:  ' + url );

	var xmlHttp = GetXMLHttpObject();
	xmlHttp.open("GET",url,false);
	xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlHttp.send(null);		//var xmlHttp = GetXMLHttpObject();
			
	document.getElementById('response_content').innerHTML = "";

  xmlDoc=xmlHttp.responseXML; 


	stringhtml = xmlHttp.responseText;
		
	//alert( 'stringhtml:' + stringhtml );
	
	//document.getElementById('response_content').innerHTML = "<pre>" + stringhtml + "</pre><br>";
	
	//return;
	
	last_xml=xmlDoc;

	this_xml = "";

 	busy=false;


	x=xmlDoc.getElementsByTagName('error');
 	
	for (i=0;i<x.length;i++)
	 {
	 try
   {
   this_xml = this_xml + "<br>";
   this_xml = this_xml + x[i].nodeName;
   this_xml = this_xml + ":";
   this_xml = this_xml + x[i].childNodes[0].nodeValue;
   this_xml = this_xml + "<br>";
 	 } 
   catch(err)
   {
   txt="There was an error parsing the xml return by api.\n\n";
   txt+="Error description: " + err.message + "\n\n";
   txt+="Click OK to continue.\n\n";
   alert(txt);
	 ++i;
   }
  }

	x=xmlDoc.getElementsByTagName('warning');
 	
	for (i=0;i<x.length;i++)
	 {
	 try
   {
   this_xml = this_xml + "<br>";
   this_xml = this_xml + x[i].nodeName;
   this_xml = this_xml + ":";
   this_xml = this_xml + x[i].childNodes[0].nodeValue;
   this_xml = this_xml + "<br>";
   alert(this_xml);
 	 } 
   catch(err)
   {
   txt="There was an error parsing the xml return by api.\n\n";
   txt+="Error description: " + err.message + "\n\n";
   txt+="Click OK to continue.\n\n";
   alert(txt);
	 ++i;
   }
  }



	
	x=xmlDoc.getElementsByTagName('record');
 	
	for (i=0;i<x.length;i++)
	 {
	 try
   {
   //this_xml = this_xml + "<br>";
   //this_xml = this_xml + x[i].nodeName;
   //this_xml = this_xml + "<br>";
   //this_xml = this_xml + "<br>";
   if ( x[i].nodeName ) debug_nodeName=x[i].nodeName;
   else debug_nodeName="*** NOT SET ***";
   last_xml = last_xml + "<br><br>parent nodename:"+debug_nodeName+"<br>";
   y=x[i].childNodes;
 	 this_xml = this_xml + "<br><div class='resultdiv'>\n";
	 for (j=0;j<y.length;j++)
	 	{
		 try
  	 {
  	 	this_value="";
    	if (( y[j].childNodes[0].length ) ) {
    		this_value = y[j].childNodes[0].nodeValue;
    	}
    	if ( this_value!="" ) {
  	 		//this_xml = this_xml + "<li class='result_li'>";
			 try
  		 {

   			if ( y[j].nodeName ) {
   				//if ( y[j].nodeName=="ObjectGUID" ) this_value = decode_base64( this_value );
   				if ( y[j].nodeName!="thumbnailPhoto" ) {
	   				this_xml = this_xml + y[j].nodeName;
	  				this_xml = this_xml + ":";
	   				this_xml = this_xml + this_value;
	   			}
	   			else {
						this_xml = this_xml + "<img src='" + this_value + "' width='150'>";
	   			}
	   			this_xml = this_xml + "</br>\n";
	   		}
			}
 			catch(err)
   		{
 			if ( y[j].nodeName ) {
 				this_xml = this_xml + y[j].nodeName;
	 			this_xml = this_xml + ":";
   			this_xml = this_xml + "[PRROBLEM PARSING]";
	   		this_xml = this_xml + "</br>\n";
	   		} else {
		   	this_xml = this_xml + "[PRROBLEM PARSING]<br>\n";
		   	++j;
				}
			}
    	} else {
   			if ( y[j].nodeName ) this_xml = this_xml + y[j].nodeName;
  			this_xml = this_xml + ":";
   			this_xml = this_xml + "[EMPTY]";
	   		this_xml = this_xml + "</br>\n";
			}

	 	} 
 		catch(err)
   	{
 			if ( y[j].nodeName ) {
 				this_xml = this_xml + y[j].nodeName;
	 			this_xml = this_xml + ":";
   			this_xml = this_xml + "[EMPTY]";
	   		this_xml = this_xml + "</br>\n";
	   	} else {
		   	this_xml = this_xml + "<br>\n";
		   	++j;
			}
		}
	}
 	 this_xml = this_xml + "</div><br>";
	} 
 catch(err)
   {
   txt="There was an error parsing the xml return by api.\n\n";
   txt+="Error description: " + err.message + "\n\n";
   txt+="Click OK to continue.\n\n";
   alert(txt);
	 ++i;
   }
  }

	//alert( 'this_xml:<pre>' + this_xml + '</pre>' );
	document.getElementById('response_content').innerHTML =  this_xml;



}

function decode_base64(s) {
    var e={},i,k,v=[],r='',w=String.fromCharCode;
    var n=[[65,91],[97,123],[48,58],[43,44],[47,48]];

    for(z in n){for(i=n[z][0];i<n[z][1];i++){v.push(w(i));}}
    for(i=0;i<64;i++){e[v[i]]=i;}

    for(i=0;i<s.length;i+=72){
    var b=0,c,x,l=0,o=s.substring(i,i+72);
         for(x=0;x<o.length;x++){
                c=e[o.charAt(x)];b=(b<<6)+c;l+=6;
                while(l>=8){r+=w((b>>>(l-=8))%256);}
         }
    }
    return r;
    }
    