<?php
// Allow from any origin
  if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
  }

// Access-Control headers are received during OPTIONS requests
  if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
      header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
  }

  error_reporting(E_ALL);
  ini_set('display_errors', 1);

  require('config.php');

  class backendAPI {

    private $db;

  //Constructor - open DB connection
    function __construct() {
      $this->db = mssql_connect(DB_HOST, DB_USER, DB_PASSWORD);
      if ($this->db) {
        mssql_select_db(DB_DATABASE, $this->db);
      }
    }

  //Destructor - close the DB connection
    function __destruct() {
      mssql_close();
    }


    function getUniqueVals($table, $col, $col2) {
      if (!isset($table) || !isset($col)) {
        die("table or col not specified");
      }
      echo "<table><tr class=\"header\">";
      $sql = "SELECT DISTINCT $col FROM offices";
      $result = mssql_query( $sql );
      while ($row = mssql_fetch_assoc($result)) {
        echo "<tr>";
        // $g = $this->lookup($row["city"] . ", " . $row["country"]);
        // $lat = "lat: " . $g["latitude"];
        // $lng = "lng: " . $g["longitude"];
        // echo "<td class=\"$col\">{$row[$col]}</td>";
        $val = str_replace("'", "''", $row[$col]);
        $sql2 = "INSERT INTO {$table} ($col2) VALUES ('$val')";
        $result2 = mssql_query( $sql2 );
        echo "<td class=\"$col\">$sql2</td>";
        echo "</tr>";
      }
      echo "</table>";
    }
  }

  $api = new backendAPI;

  $table = $_GET['table'];
  $col = $_GET['col'];
  $col2 = $_GET['col2'];

  $api->getUniqueVals($table, $col, $col2);

 
?>
