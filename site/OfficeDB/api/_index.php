<?php
// Allow from any origin
  if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
  }

// Access-Control headers are received during OPTIONS requests
  if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
      header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
  }

  error_reporting(E_ALL);
  ini_set('display_errors', 1);
  ini_set('max_input_vars', 3000);

  require('config.php');

  class backendAPI {

    private $db;
    private $staticFields = array('id', 'region', 'country');

  //Constructor - open DB connection
    function __construct() {
      $this->db = mssql_connect(DB_HOST, DB_USER, DB_PASSWORD);
      if ($this->db) {
        mssql_select_db(DB_DATABASE, $this->db);
      }
    }

  //Destructor - close the DB connection
    function __destruct() {
      mssql_close();
    }



  //Get all from the specifed table
    function getAll($table) {
      // $sql = "SELECT * FROM " . $table;
      $sql = "SELECT DISTINCT * FROM offices";
      $sql .= " INNER JOIN (SELECT country, boundsNElatitude AS c_NELat, boundsNElongitude AS c_NELng, boundsSWlatitude AS c_SWLat, boundsSWlongitude AS c_SWLng FROM countries)";
      $sql .= " c ON offices.country=c.country";
      $sql .= " INNER JOIN (SELECT region, boundsNElatitude AS r_NELat, boundsNElongitude AS r_NELng, boundsSWlatitude AS r_SWLat, boundsSWlongitude AS r_SWLng, contactPhone, contactEmail, displayOrder FROM regions)";
      $sql .= " r ON offices.region=r.region";
      $sql .= " ORDER BY offices.id;";
      $result=mssql_query( $sql );

      while (($row = mssql_fetch_assoc($result))) {
        foreach($row as $key => $val)
        {
          $c[$key] = utf8_encode($val);
        }
        $resultArray[] = $c;
        // $result[] = json_encode($row);
      }

      echo json_encode($resultArray);
    }



   //Get a specific row from the table
    function getSingle($table, $id) {
      $sql = "SELECT * FROM " . $table ." WHERE id=?";
      $stmt = $this->db->prepare($sql);
      $stmt->bind_param('i', $id);
      $stmt->execute();
      $meta = $stmt->result_metadata();
      while ($field = $meta->fetch_field())
      {
        $params[] = &$row[$field->name];
      }

      call_user_func_array(array($stmt, 'bind_result'), $params);

      while ($stmt->fetch()) {
        foreach($row as $key => $val)
        {
          $c[$key] = $val;
        }
        $result[] = $c;
      }
      $stmt->close();
      echo json_encode($result);
    }

  }


  //GET data
  if(isset($_GET['table'])) {

    $table = $_GET['table'];

    if($table != null) {
      $api = new backendAPI;

      if(isset($_GET['action']) && $_GET['action'] == 'getJson') {
        // echo "getJson";
        //If there is an ID in the query string, return that user
        if(isset($_GET['id'])) {
          header('Content-Type: application/json');
          $api->getSingle($table, $_GET['id']);
          exit(0);
        } else {
          header('Content-Type: application/json');
          $api->getAll($table);
          exit(0);
        }
      }
    }
  }

  $action = isset($_GET['action']) ? $_GET['action'] : '';
  $table = isset($_GET['table']) ? $_GET['table'] : '';

?>