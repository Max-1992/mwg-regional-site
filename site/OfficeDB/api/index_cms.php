<?php
// Allow from any origin
  if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
  }

// Access-Control headers are received during OPTIONS requests
  if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
      header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
  }

  error_reporting(E_ALL);
  ini_set('display_errors', 1);
  ini_set('max_input_vars', 3000);

  require('config.php');

  class backendAPI {

    private $db;
    private $staticFields = array('id', 'region', 'country');

  //Constructor - open DB connection
    function __construct() {
      ini_set('mssql.charset', 'UTF-8');
      $this->db = mssql_connect(DB_HOST, DB_USER, DB_PASSWORD);
      if ($this->db) {
        mssql_select_db(DB_DATABASE, $this->db);
      }
    }

  //Destructor - close the DB connection
    function __destruct() {
      mssql_close();
    }

  //Get all from the specifed table and show in HTML table
    function getAllHTML($table) {
      // $sql = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='" . $table . "'";
      $sql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '" . $table . "'";
      $result = mssql_query($sql);
      echo "<div class=\"table-wrapper\"><table class=\"table table-striped table-hover table-responsive\"><tr class=\"header\">";
      while ($row = mssql_fetch_assoc($result)) {
        foreach($row as $key => $val) {
          echo "<th>" . utf8_encode($val) . "</th>";
        }
      }
      echo "</tr>";

      $top10 = isset($_GET['top10']) ? "TOP 10" : "";
      $sql = "SELECT " . $top10 . " * FROM " . $table;
      // $sql = "SELECT TOP 10 * FROM " . $table;
      $result = mssql_query( $sql );
      while ($row = mssql_fetch_assoc($result)) {
        echo "<tr id=\"{$row['id']}\">";
        // $g = $this->lookup($row["city"] . ", " . $row["country"]);
        // $lat = "lat: " . $g["latitude"];
        // $lng = "lng: " . $g["longitude"];
        foreach($row as $key => $val) {
          $isLatLngField = strpos($key, "atitude") !== false || strpos($key, "ongitude") !== false;
          $noValue = trim($val) == '';
          if ($isLatLngField && $noValue) {
            echo "<td class=\"$key\"><span>$val</span><input type=\"hidden\" name=\"{$row['id']}-$key\" value=\"$val\"></td>";
          } else if (in_array($key, $this->staticFields)) {
            echo "<td class=\"$key\">$val</td>";
          } else {
            echo "<td class=\"$key\"><a data-name=\"$key\" data-id=\"$table:{$row['id']}\" href=\"#\">" . utf8_encode($val) . "</a></td>";
          }
        }
        echo "</tr>";
      }
      echo "</table>";
    }

  //Get all from the specifed table
    function getAll($table) {
      // $sql = "SELECT * FROM " . $table;
      $sql = "SELECT DISTINCT * FROM offices";
      $sql .= " INNER JOIN (SELECT country, boundsNElatitude AS c_NELat, boundsNElongitude AS c_NELng, boundsSWlatitude AS c_SWLat, boundsSWlongitude AS c_SWLng FROM countries)";
      $sql .= " c ON offices.country=c.country";
      $sql .= " INNER JOIN (SELECT region, boundsNElatitude AS r_NELat, boundsNElongitude AS r_NELng, boundsSWlatitude AS r_SWLat, boundsSWlongitude AS r_SWLng, contactPhone, contactEmail, displayOrder FROM regions)";
      $sql .= " r ON offices.region=r.region";
      $sql .= " ORDER BY offices.id;";
      $result=mssql_query( $sql );

      while (($row = mssql_fetch_assoc($result))) {
        foreach($row as $key => $val)
        {
          $c[$key] = utf8_encode($val);
        }
        $resultArray[] = $c;
        // $result[] = json_encode($row);
      }

      echo json_encode($resultArray);
    }

    // Save lat/lng data
    function saveLatLng($table) {
      // $sql = "UPDATE $table SET";
      // foreach ($_POST as $key => $value) {
      //   if ($key != 'action' && $key != 'table') {
      //     $id = explode("-", $key)[0];
      //     $field = explode("-", $key)[1];
      //     echo $sql;
      //     $sql = "UPDATE $table SET $field='$value' WHERE id='$id';";
      //     $result = mssql_query($sql);
      //   }
      // }
      echo "OK";
    }

    // Save lat/lng data
    function deleteLatLng($table, $id) {
      // die($table);
      if ($table == "regions" || $table == "countries") {
        $sql = "UPDATE $table SET boundsNELatitude='', boundsNELongitude='', boundsSWLatitude='', boundsSWLongitude='' WHERE id='$id'";
      } else {
        $sql = "UPDATE $table SET latitude='', longitude='' WHERE id='$id'";
      }
      echo $sql;
      $result = mssql_query($sql);
      echo "OK";
    }

    // update field
    function save($table, $id, $name, $value) {
      $sql = "UPDATE $table SET $name='" . utf8_encode($value) . "' WHERE id='$id'";
      echo $sql;
      $result = mssql_query($sql);
      echo "OK";
    }

   //Get a specific row from the table
    function getSingle($table, $id) {
      $sql = "SELECT * FROM " . $table ." WHERE id=?";
      $stmt = $this->db->prepare($sql);
      $stmt->bind_param('i', $id);
      $stmt->execute();
      $meta = $stmt->result_metadata();
      while ($field = $meta->fetch_field())
      {
        $params[] = &$row[$field->name];
      }

      call_user_func_array(array($stmt, 'bind_result'), $params);

      while ($stmt->fetch()) {
        foreach($row as $key => $val)
        {
          $c[$key] = $val;
        }
        $result[] = $c;
      }
      $stmt->close();
      echo json_encode($result);
    }

  }
//POST data
  $postdata = file_get_contents("php://input");
  $request = json_decode($postdata);
  //POST data
    // print_r($_POST);
  // echo $_POST['table'];
  if(isset($_POST['table'])) {
    // print_r($_POST['table']);
    // die('yo');

    $table = $_POST['table'];

    if($table != null) {
      $api = new backendAPI;

      if(isset($_POST['action'])) {
        $action = $_POST['action'];

        if ($action == 'saveLatLng') {
          $api->saveLatLng($table);
          exit(0);
        } else if ($action == 'deleteLatLng') {
          echo "we are here";
          $id = $_POST['id'];
          $api->deleteLatLng($table, $id);
          exit(0);
        } else if ($action == 'edit') {
          die('save');
          $id = $_POST['pk'];
          $name = $_POST['name'];
          $value = $_POST['value'];
          $api->save($table, $id, $name, $value);
          exit(0);
        } else {
          echo "unknown action";
          exit(0);
        }
      }
    }
  }

  //GET data
  if(isset($_GET['table'])) {

    $table = $_GET['table'];

    if($table != null) {
      $api = new backendAPI;

      if(isset($_GET['action']) && $_GET['action'] == 'getJson') {
        // echo "getJson";
        //If there is an ID in the query string, return that user
        if(isset($_GET['id'])) {
          header('Content-Type: application/json');
          $api->getSingle($table, $_GET['id']);
          exit(0);
        } else {
          header('Content-Type: application/json');
          $api->getAll($table);
          exit(0);
        }
      }
    }
  }

  $action = isset($_GET['action']) ? $_GET['action'] : '';
  $table = isset($_GET['table']) ? $_GET['table'] : '';

?>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
<link href="./styles.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
</head>
<body>
<ul class="nav nav-tabs">
  <li<?= $table === "regions" ? " class=\"active\"" : "" ?>><a href="?table=regions&action=geocode">Regions</a></li>
  <li<?= $table === "countries" ? " class=\"active\"" : "" ?>><a href="?table=countries&action=geocode">Countries</a></li>
  <li<?= strpos($table, "offices") !== false ? " class=\"active\"" : "" ?>><a href="?table=offices&action=geocode">Office Locations</a></li>
  <li<?= $table === "" ? " class=\"hidden\"" : "" ?>>
    <div class="geocode">
      <div id="geocode-get">
        <button class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="bottom" title="Get lat/lng data from Google for empty fields">get geocode data</button>
        <span class="status"></span>
      </div>
      <div id="geocode-save">
        <button class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="bottom" title="Save all lat/lng data">save geocode data</button>
        <span class="status"></span>
      </div>
      <div id="geocode-delete">
        id: <input type="number" name="delete-id">
        <button class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="bottom" title="Delete lat/lng data for this id">delete geocode data</button>
        <span class="status"></span>
      </div>
    </div>
  </li>
</ul>

<?php
//Verify that there is a table variable and that it matches one of our actual tables
  if(isset($_GET['table'])) {

    $table = $_GET['table'];

    if($table != null) {
      $api = new backendAPI;

      if(isset($_GET['action']) && $_GET['action'] == 'geocode') {
        $api->getAllHTML($table);
      } else {
        echo "something else";
      }
    }
?>


<script type="text/javascript">

  var serviceURL = '<?php echo $_SERVER['PHP_SELF']; ?>';

  $('[data-toggle="tooltip"]').tooltip();

  $('#geocode-delete button').on('click', function() {
    var id = $(this).parent().find('input').val();
    if (!id) {
      console.log('No id specified');
      return;
    }
    $.ajax({
      url: serviceURL,
      method: 'post',
      data: {
        action: 'deleteLatLng',
        table: '<?php echo $table?>',
        id: id
      }
    }).done(function(data) {
      console.log(data);
      alert('Successfully deleted lat/lng values for id ' + id + '.');

      // document.location.reload();
    });
  });

  $('#geocode-save button').on('click', function() {
    var $status = $(this).parent().find('.status');
    var total = 0;
    var postData = $('table input').serialize() + '&action=saveLatLng&table=<?php echo $table?>';
    $.ajax({
      url: serviceURL,
      method: 'post',
      data: postData
    }).done(function(data) {
      console.log(data);
      alert('Successfully saved lat/lng values.');
      // document.location.reload();
    });
  });

  $('#geocode-get button').on('click', function() {
    var $status = $(this).parent().find('.status');
    var total = 0;
    $('table tr:not(.header)').each(function(i, item) {
      var $this = $(this);
      var city = $this.find('td.city').text();
      var country = $this.find('td.country').text();
      var region = $this.find('td.region').text();
      var $id = $this.find('td.id');
      var $latitude = $this.find('td.latitude');
      var $longitude = $this.find('td.longitude');
      var $boundsNElatitude = $this.find('td.boundsNElatitude');
      var $boundsNElongitude = $this.find('td.boundsNElongitude');
      var $boundsSWlatitude = $this.find('td.boundsSWlatitude');
      var $boundsSWlongitude = $this.find('td.boundsSWlongitude');
      var address = city + ', ' + country;
      var latVal = $latitude.find('input').length ? $latitude.find('input').val() : $latitude.find('a').text();
      var lngVal = $longitude.find('input').length ? $longitude.find('input').val() : $longitude.find('a').text();
      var latValNE = $boundsNElatitude.find('input').length ? $boundsNElatitude.find('input').val() : $boundsNElatitude.find('a').text();
      var lngValNE = $boundsNElongitude.find('input').length ? $boundsNElongitude.find('input').val() : $boundsNElongitude.find('a').text();
      var latValSW = $boundsSWlatitude.find('input').length ? $boundsSWlatitude.find('input').val() : $boundsSWlatitude.find('a').text();
      var lngValSW = $boundsSWlongitude.find('input').length ? $boundsSWlongitude.find('input').val() : $boundsSWlongitude.find('a').text();
      // console.log(latVal,lngVal,latValNE,lngValNE,latValSW,lngValSW);
      if ('<?php echo $table?>' === 'countries') {
        address = country;
      } else if ('<?php echo $table?>' === 'regions') {
        address = region;
      }
      var url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&key=AIzaSyBNT4aSy75bVQIUmlqXr9sD0GcyRfYnT9Q';

      if (($latitude.length && $longitude.length) && latVal.trim() && lngVal.trim()) {
        console.log('lat/lng data exists for', city);
        return;
      }
      if (($boundsNElatitude.length && $boundsNElongitude.length) && latValNE.trim() && lngValNE.trim()) {
        console.log('NE lat/lng data exists for', country || region);
        return;
      }
      if (($boundsSWlatitude.length && $boundsSWlongitude.length) && latValSW.trim() && lngValSW.trim()) {
        console.log('SW lat/lng data exists for', country || region);
        return;
      }
      // console.log(address);
      $.ajax({
        url: url
      }).done(function(data) {
        if (data.status === 'OK') {
          // console.log($id.text(), 'done', data.results[0].geometry.location.lat, data.results[0].geometry.location.lng);
          $status.text('processed ' + (++total) + ' entries.');
          if ('<?php echo $table?>'.match('offices')) {
            $latitude.find('span').text(data.results[0].geometry.location.lat);
            $latitude.find('input').val(data.results[0].geometry.location.lat);
            $longitude.find('span').text(data.results[0].geometry.location.lng);
            $longitude.find('input').val(data.results[0].geometry.location.lng);
          } else {
            $boundsNElatitude.find('span').text(data.results[0].geometry.bounds.northeast.lat);
            $boundsNElatitude.find('input').val(data.results[0].geometry.bounds.northeast.lat);
            $boundsNElongitude.find('span').text(data.results[0].geometry.bounds.northeast.lng);
            $boundsNElongitude.find('input').val(data.results[0].geometry.bounds.northeast.lng);
            $boundsSWlatitude.find('span').text(data.results[0].geometry.bounds.southwest.lat);
            $boundsSWlatitude.find('input').val(data.results[0].geometry.bounds.southwest.lat);
            $boundsSWlongitude.find('span').text(data.results[0].geometry.bounds.southwest.lng);
            $boundsSWlongitude.find('input').val(data.results[0].geometry.bounds.southwest.lng);
          }
          // console.log('done', data.results[0].geometry.location);
        } else {
          console.log($id, data.status);
        }
      });
    });
  });

  $('td a').each(function(_i, _item) {
    var $this = $(this);
    var type = $this.data('name');
    var pk = $this.data('id').split(':')[1];
    var table = $this.data('id').split(':')[0];
    var title = $(this).data('name');
    var table = '<?= $table ?>';

    $this.editable({
      // 'type': type,
      // 'source': [{"value":"1","text":"GAF Commercial Solar Program"},{"value":"5","text":"Press Releases"}],
      'defautValue': null,
      // 'tpl': tpl,
      'pk': pk,
      'params': {
        'action': 'edit',
        'table': table
      },
      'title': title,
      'clear': false,
      'inputclass': type === 'select' ? 'input-large' : 'imput-sm',
      'success': successHandler,
      'url': serviceURL
    });
  });

  var successHandler = function successHandler(response) {
    console.log(response);
  }
</script>
<?php
  } else {
    echo "<div class=\"table-wrapper\"><h5>Select a category above</h5></div>";
  }
?>

</body>
</html>