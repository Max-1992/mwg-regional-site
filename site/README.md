# McCann Worldgroup Website mccannworldgroup.com
This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.14.0.

## Installation

Install node: [download](https://nodejs.org/en/download/)

Install grunt: 'npm install -g grunt-cli'

Install project dependencies: 'npm install'

## Build & development

Run `grunt serve` to start watchers for LESS files and JS/HTML files.  This will start a local server and open the site in your default browser.  Saving files should auto refresh browser.

## Build for deployment
Run 'grunt build' to create a build in the stage directory
Run 'grunt serve:stage' to build and then launch the build in a browser for testing


Backend located here: https://cms.mccannworldgroup.com/wp-admin/
