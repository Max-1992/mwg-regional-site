'use strict';

describe('Service: mwgData', function () {

  // load the service's module
  beforeEach(module('mwgApp'));

  // instantiate service
  var mwgData;
  beforeEach(inject(function (_mwgData_) {
    mwgData = _mwgData_;
  }));

  it('should do something', function () {
    expect(!!mwgData).toBe(true);
  });

});
