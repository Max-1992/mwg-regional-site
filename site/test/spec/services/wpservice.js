'use strict';

describe('Service: wpservice', function () {

  // load the service's module
  beforeEach(module('mwgApp'));

  // instantiate service
  var wpservice;
  beforeEach(inject(function (_wpservice_) {
    wpservice = _wpservice_;
  }));

  it('should do something', function () {
    expect(!!wpservice).toBe(true);
  });

});
