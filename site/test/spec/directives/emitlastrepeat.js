'use strict';

describe('Directive: emitLastRepeat', function () {

  // load the directive's module
  beforeEach(module('mwgApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<emit-last-repeat></emit-last-repeat>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the emitLastRepeat directive');
  }));
});
