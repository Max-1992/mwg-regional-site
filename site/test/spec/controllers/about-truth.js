'use strict';

describe('Controller: AboutTruthCtrl', function () {

  // load the controller's module
  beforeEach(module('mwgApp'));

  var AboutTruthCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AboutTruthCtrl = $controller('AboutTruthCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
