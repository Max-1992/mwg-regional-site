'use strict';

describe('Controller: AboutPeopleDetailCtrl', function () {

  // load the controller's module
  beforeEach(module('mwgApp'));

  var AboutPeopleDetailCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AboutPeopleDetailCtrl = $controller('AboutPeopleDetailCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
