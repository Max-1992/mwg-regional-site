'use strict';

describe('Controller: AboutPeopleCtrl', function () {

  // load the controller's module
  beforeEach(module('mwgApp'));

  var AboutPeopleCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AboutPeopleCtrl = $controller('AboutPeopleCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
