'use strict';

describe('Controller: CareersCtrl', function () {

  // load the controller's module
  beforeEach(module('mwgApp'));

  var CareersCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CareersCtrl = $controller('CareersCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(CareersCtrl.awesomeThings.length).toBe(3);
  });
});
