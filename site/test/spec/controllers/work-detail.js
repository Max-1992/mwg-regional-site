'use strict';

describe('Controller: WorkDetailCtrl', function () {

  // load the controller's module
  beforeEach(module('mccannworldgroupcomApp'));

  var WorkDetailCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    WorkDetailCtrl = $controller('WorkDetailCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
