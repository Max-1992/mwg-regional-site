'use strict';

describe('Controller: WorkBrandCtrl', function () {

  // load the controller's module
  beforeEach(module('mccannworldgroupcomApp'));

  var WorkBrandCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    WorkBrandCtrl = $controller('WorkBrandCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
