// Karma configuration
// http://karma-runner.github.io/0.12/config/configuration-file.html
// Generated on 2017-01-23 using
// generator-karma 1.0.0

module.exports = function(config) {
  'use strict';

  config.set({
    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,

    // base path, that will be used to resolve files and exclude
    basePath: '../',

    // testing framework to use (jasmine/mocha/qunit/...)
    // as well as any additional frameworks (requirejs/chai/sinon/...)
    frameworks: [
      "jasmine"
    ],

    // list of files / patterns to load in the browser
    files: [
      // bower:js
      'dev/bower_components/jquery/dist/jquery.js',
      'dev/bower_components/angular/angular.js',
      'dev/bower_components/angular-animate/angular-animate.js',
      'dev/bower_components/angular-cookies/angular-cookies.js',
      'dev/bower_components/angular-resource/angular-resource.js',
      'dev/bower_components/angular-route/angular-route.js',
      'dev/bower_components/angular-sanitize/angular-sanitize.js',
      'dev/bower_components/angular-touch/angular-touch.js',
      'dev/bower_components/owl.carousel/dist/owl.carousel.js',
      'dev/bower_components/scrollmagic/scrollmagic/uncompressed/ScrollMagic.js',
      'dev/bower_components/gsap/src/uncompressed/TweenMax.js',
      'dev/bower_components/angular-scroll-magic/angular-scroll-magic.js',
      'dev/bower_components/underscore/underscore.js',
      'dev/bower_components/bLazy/blazy.js',
      'dev/bower_components/svg4everybody/dist/svg4everybody.js',
      'dev/bower_components/angular-bind-html-compile/angular-bind-html-compile.js',
      'dev/bower_components/angular-scroll/angular-scroll.js',
      'dev/bower_components/angular-mocks/angular-mocks.js',
      // endbower
      "app/scripts/**/*.js",
      "test/mock/**/*.js",
      "test/spec/**/*.js"
    ],

    // list of files / patterns to exclude
    exclude: [
    ],

    // web server port
    port: 8080,

    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: [
      "PhantomJS"
    ],

    // Which plugins to enable
    plugins: [
      "karma-phantomjs-launcher",
      "karma-jasmine"
    ],

    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false,

    colors: true,

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO,

    // Uncomment the following lines if you are using grunt's server to run the tests
    // proxies: {
    //   '/': 'http://localhost:9000/'
    // },
    // URL root prevent conflicts with the site root
    // urlRoot: '_karma_'
  });
};
