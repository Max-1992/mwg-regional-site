<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage McCann_WG
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-info">
	<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'mccannwg' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'mccannwg' ), 'WordPress' ); ?></a>
</div><!-- .site-info -->
